import ElectronService from '../../../../src/shared/ElectronService/ElectronService';

let mockGetCurrentWindow: jest.Mock;
const mockMaximize = jest.fn();
const mockUnmaximize = jest.fn();
const mockMinimize = jest.fn();
const mockClose = jest.fn();
const mockCurrentWindow = {
  maximize: mockMaximize,
  unmaximize: mockUnmaximize,
  minimize: mockMinimize,
  close: mockClose
};

jest.mock('electron', () => {
  mockGetCurrentWindow = jest.fn();
  return {
    remote: {
      getCurrentWindow: mockGetCurrentWindow
    }
  };
});

describe('ElectronService', () => {
  beforeEach(() => {
    jest.clearAllMocks();
    mockGetCurrentWindow.mockReturnValue(mockCurrentWindow);
  });

  describe('currentWindow', () => {
    it('should return the current window', () => {
      const electronService = new ElectronService();
      expect(electronService.currentWindow).toBe(mockCurrentWindow);
    });
  });

  describe('maximizeWindow', () => {
    it('should maximize current window', () => {
      const electronService = new ElectronService();
      electronService.maximizeWindow();
      expect(mockMaximize).toHaveBeenCalled();
    });
  });

  describe('unmaximizeWindow', () => {
    it('should unmaximize current window', () => {
      const electronService = new ElectronService();
      electronService.unmaximizeWindow();
      expect(mockUnmaximize).toHaveBeenCalled();
    });
  });

  describe('minimizeWindow', () => {
    it('should unmaximize current window', () => {
      const electronService = new ElectronService();
      electronService.minimizeWindow();
      expect(mockMinimize).toHaveBeenCalled();
    });
  });

  describe('closeWindow', () => {
    it('should unmaximize current window', () => {
      const electronService = new ElectronService();
      electronService.closeWindow();
      expect(mockClose).toHaveBeenCalled();
    });
  });
});
