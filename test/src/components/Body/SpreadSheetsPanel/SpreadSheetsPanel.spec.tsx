import React from 'react';
import SpreadSheetsPanel from '../../../../../src/components/Body/SpreadSheetsPanel/SpreadSheetsPanel';
import { shallow } from 'enzyme';
import { FileSectionType } from '../../../../../src/components/Body/SpreadSheetsPanel/FileSection/FileSection';

// eslint-disable-next-line @typescript-eslint/no-explicit-any
let MockFileSection: any;
jest.mock(
  '../../../../../src/components/Body/SpreadSheetsPanel/FileSection/FileSection',
  () => {
    // eslint-disable-next-line react/display-name
    MockFileSection = () => <></>;
    return {
      ...(jest.requireActual(
        '../../../../../src/components/Body/SpreadSheetsPanel/FileSection/FileSection'
        // eslint-disable-next-line @typescript-eslint/no-explicit-any
      ) as any),
      __esModule: true,
      default: MockFileSection
    };
  }
);

// eslint-disable-next-line @typescript-eslint/no-explicit-any
let MockCompareButton: any;
jest.mock(
  '../../../../../src/common/buttons/ExecuteComparisonButton/ExecuteComparisonButton',
  () => {
    // eslint-disable-next-line react/display-name
    MockCompareButton = () => <></>;
    return {
      __esModule: true,
      default: MockCompareButton
    };
  }
);

describe('SpreadSheetsPanel', () => {
  beforeEach(() => {
    jest.resetAllMocks();
  });

  it('should render 2 file section components', () => {
    const wrapper = shallow(<SpreadSheetsPanel />);

    expect(wrapper.find(MockFileSection)).toHaveLength(2);
  });

  it('should pass in MASTER prop type to first component', () => {
    const wrapper = shallow(<SpreadSheetsPanel />);

    const FileSections = wrapper.find(MockFileSection);
    const MasterFileSection = FileSections.at(0);

    // eslint-disable-next-line @typescript-eslint/no-explicit-any
    expect((MasterFileSection.props() as any).type).toEqual(
      FileSectionType.MASTER
    );
  });

  it('should pass in CHILDREN prop type to second component', () => {
    const wrapper = shallow(<SpreadSheetsPanel />);

    const FileSections = wrapper.find(MockFileSection);
    const ChildFileSection = FileSections.at(1);

    // eslint-disable-next-line @typescript-eslint/no-explicit-any
    expect((ChildFileSection.props() as any).type).toEqual(
      FileSectionType.CHILDREN
    );
  });

  it('should render ExecuteComparisonButton', () => {
    const wrapper = shallow(<SpreadSheetsPanel />);

    expect(wrapper.find(MockCompareButton)).toHaveLength(1);
  });

  it('ExecuteComparisonButton should include label', () => {
    const wrapper = shallow(<SpreadSheetsPanel />);

    // eslint-disable-next-line @typescript-eslint/no-explicit-any
    expect((wrapper.find(MockCompareButton).props() as any).includeLabel).toBe(
      true
    );
  });
});
