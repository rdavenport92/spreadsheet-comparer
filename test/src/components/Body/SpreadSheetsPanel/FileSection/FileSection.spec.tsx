import React from 'react';
import FileSection, {
  FileSectionType
} from '../../../../../../src/components/Body/SpreadSheetsPanel/FileSection/FileSection';
import { mount, render, shallow } from 'enzyme';

// eslint-disable-next-line @typescript-eslint/no-explicit-any
let MockHr: any;
jest.mock('../../../../../../src/common/Hr/Hr', () => {
  // eslint-disable-next-line react/display-name
  MockHr = () => <></>;
  return {
    __esModule: true,
    default: MockHr
  };
});

// eslint-disable-next-line @typescript-eslint/no-explicit-any
let MockDropzone: any;
jest.mock('../../../../../../src/common/Dropzone/Dropzone', () => {
  // eslint-disable-next-line react/display-name
  MockDropzone = () => <></>;
  return {
    __esModule: true,
    default: MockDropzone
  };
});

describe('FileSection', () => {
  beforeEach(() => {
    jest.resetAllMocks();
  });

  it('should have type prop', () => {
    const wrapper = mount(<FileSection type={FileSectionType.MASTER} />);

    // eslint-disable-next-line @typescript-eslint/no-explicit-any
    expect((wrapper.props() as any).type).toEqual(FileSectionType.MASTER);
  });

  it('should display appropriate title header for type Master', () => {
    const wrapper = render(<FileSection type={FileSectionType.MASTER} />);

    expect(wrapper.find('.file-section-header').text()).toEqual('Master');
  });

  it('should display appropriate title header for type Children', () => {
    const wrapper = render(<FileSection type={FileSectionType.CHILDREN} />);

    expect(wrapper.find('.file-section-header').text()).toEqual('Children');
  });

  it('should render an Hr component', () => {
    const wrapper = shallow(<FileSection type={FileSectionType.CHILDREN} />);

    expect(wrapper.find(MockHr)).toHaveLength(1);
  });

  it('should render dropzone if type Master', () => {
    const wrapper = shallow(<FileSection type={FileSectionType.MASTER} />);

    expect(wrapper.find(MockDropzone)).toHaveLength(1);
  });
});
