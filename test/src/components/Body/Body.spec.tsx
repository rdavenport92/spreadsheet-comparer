import React from 'react';
import Body from '../../../../src/components/Body/Body';
import { shallow } from 'enzyme';

// eslint-disable-next-line @typescript-eslint/no-explicit-any
let MockSectionPanel: any;
jest.mock('../../../../src/components/Body/SectionPanel/SectionPanel', () => {
  // eslint-disable-next-line react/display-name
  MockSectionPanel = () => <></>;
  return {
    __esModule: true,
    default: MockSectionPanel
  };
});

// eslint-disable-next-line @typescript-eslint/no-explicit-any
let MockSpreadSheetsPanel: any;
jest.mock(
  '../../../../src/components/Body/SpreadSheetsPanel/SpreadSheetsPanel',
  () => {
    // eslint-disable-next-line react/display-name
    MockSpreadSheetsPanel = () => <></>;
    return {
      __esModule: true,
      default: MockSpreadSheetsPanel
    };
  }
);

describe('Body', () => {
  beforeEach(() => {
    jest.resetAllMocks();
  });

  it('should render SectionPanel', () => {
    const wrapper = shallow(<Body />);

    expect(wrapper.find(MockSectionPanel)).toHaveLength(1);
  });

  it('should pass "Spreadsheets" into title prop for SectionPanel', () => {
    const wrapper = shallow(<Body />);

    const SectionPanel = wrapper.find(MockSectionPanel);
    // eslint-disable-next-line @typescript-eslint/no-explicit-any
    expect((SectionPanel.props() as any).title).toEqual('Spreadsheets');
  });

  it('should render SpreadSheetsPanel', () => {
    const wrapper = shallow(<Body />);

    expect(wrapper.find(MockSpreadSheetsPanel)).toHaveLength(1);
  });
});
