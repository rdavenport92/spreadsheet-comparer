import React from 'react';
import SectionPanel from '../../../../../src/components/Body/SectionPanel/SectionPanel';
import { mount, render, shallow } from 'enzyme';

describe('SectionPanel', () => {
  beforeEach(() => {
    jest.resetAllMocks();
  });

  it('should receive title prop', () => {
    const wrapper = mount(<SectionPanel title="test" />);

    // eslint-disable-next-line @typescript-eslint/no-explicit-any
    expect((wrapper.props() as any).title).toEqual('test');
  });

  it('should display title in section-panel-header', () => {
    const wrapper = render(<SectionPanel title="test" />);

    expect(wrapper.find('.section-panel-header').text()).toEqual('test');
  });

  it('renders children components', () => {
    const TestComponent = () => <div data-testid="test-child" />;
    const wrapper = shallow(
      <SectionPanel title="test">
        <TestComponent />
      </SectionPanel>
    );

    expect(wrapper.find(TestComponent)).toHaveLength(1);
  });
});
