import React from 'react';
import Header from '../../../../src/components/Header/Header';
import { shallow } from 'enzyme';

// eslint-disable-next-line @typescript-eslint/no-explicit-any
let MockMenuButton: any;
jest.mock('../../../../src/common/buttons/MenuButton/MenuButton', () => {
  // eslint-disable-next-line react/display-name
  MockMenuButton = () => <></>;
  return {
    __esModule: true,
    default: MockMenuButton
  };
});

// eslint-disable-next-line @typescript-eslint/no-explicit-any
let MockSaveTemplateButton: any;
jest.mock(
  '../../../../src/common/buttons/SaveTemplateButton/SaveTemplateButton',
  () => {
    // eslint-disable-next-line react/display-name
    MockSaveTemplateButton = () => <></>;
    return {
      __esModule: true,
      default: MockSaveTemplateButton
    };
  }
);

// eslint-disable-next-line @typescript-eslint/no-explicit-any
let MockLoadTemplateButton: any;
jest.mock(
  '../../../../src/common/buttons/LoadTemplateButton/LoadTemplateButton',
  () => {
    // eslint-disable-next-line react/display-name
    MockLoadTemplateButton = () => <></>;
    return {
      __esModule: true,
      default: MockLoadTemplateButton
    };
  }
);

// eslint-disable-next-line @typescript-eslint/no-explicit-any
let MockExecuteComparisonButton: any;
jest.mock(
  '../../../../src/common/buttons/ExecuteComparisonButton/ExecuteComparisonButton',
  () => {
    // eslint-disable-next-line react/display-name
    MockExecuteComparisonButton = () => <></>;
    return {
      __esModule: true,
      default: MockExecuteComparisonButton
    };
  }
);

describe('Header', () => {
  beforeEach(() => {
    jest.resetAllMocks();
  });

  it('should render MenuButton', () => {
    const wrapper = shallow(<Header />);

    expect(wrapper.find(MockMenuButton)).toHaveLength(1);
  });

  it('should render SaveTemplateButton', () => {
    const wrapper = shallow(<Header />);

    expect(wrapper.find(MockSaveTemplateButton)).toHaveLength(1);
  });

  it('should render LoadTemplateButton', () => {
    const wrapper = shallow(<Header />);

    expect(wrapper.find(MockLoadTemplateButton)).toHaveLength(1);
  });

  it('should render ExecuteComparisonButton', () => {
    const wrapper = shallow(<Header />);

    expect(wrapper.find(MockExecuteComparisonButton)).toHaveLength(1);
  });

  it('should wrap save, load and execute in button-wrapper-right divs', () => {
    const wrapper = shallow(<Header />);

    const divs = wrapper.find('.button-wrapper-right');
    const div1 = divs.at(0);
    const div2 = divs.at(1);
    const div3 = divs.at(2);

    expect(div1.find(MockSaveTemplateButton)).toHaveLength(1);
    expect(div2.find(MockLoadTemplateButton)).toHaveLength(1);
    expect(div3.find(MockExecuteComparisonButton)).toHaveLength(1);
  });
});
