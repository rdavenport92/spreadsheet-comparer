import React from 'react';
import { shallow } from 'enzyme';
import Win32Controls from '../../../../../src/components/Toolbar/Win32Controls/Win32Controls';

// eslint-disable-next-line @typescript-eslint/no-explicit-any
let MockMinButton: any;
jest.mock(
  '../../../../../src/common/buttons/win32/Win32MinButton/Win32MinButton',
  () => {
    // eslint-disable-next-line react/display-name
    MockMinButton = () => <></>;
    return {
      __esModule: true,
      default: MockMinButton
    };
  }
);

// eslint-disable-next-line @typescript-eslint/no-explicit-any
let MockUnmaxButton: any;
jest.mock(
  '../../../../../src/common/buttons/win32/Win32UnmaxButton/Win32UnmaxButton',
  () => {
    // eslint-disable-next-line react/display-name
    MockUnmaxButton = () => <></>;
    return {
      __esModule: true,
      default: MockUnmaxButton
    };
  }
);

// eslint-disable-next-line @typescript-eslint/no-explicit-any
let MockMaxButton: any;
jest.mock(
  '../../../../../src/common/buttons/win32/Win32MaxButton/Win32MaxButton',
  () => {
    // eslint-disable-next-line react/display-name
    MockMaxButton = () => <></>;
    return {
      __esModule: true,
      default: MockMaxButton
    };
  }
);

// eslint-disable-next-line @typescript-eslint/no-explicit-any
let MockCloseButton: any;
jest.mock(
  '../../../../../src/common/buttons/win32/Win32CloseButton/Win32CloseButton',
  () => {
    // eslint-disable-next-line react/display-name
    MockCloseButton = () => <></>;
    return {
      __esModule: true,
      default: MockCloseButton
    };
  }
);

let mockUseSelector: jest.Mock;
jest.mock('react-redux', () => {
  mockUseSelector = jest.fn();
  return {
    useSelector: mockUseSelector
  };
});

let mockGetIsScreenMaximized: jest.Mock;
jest.mock('../../../../../src/common/redux/mainSlice', () => {
  mockGetIsScreenMaximized = jest.fn();
  return {
    getIsScreenMaximized: mockGetIsScreenMaximized
  };
});

describe('Win32Controls', () => {
  beforeEach(() => {
    jest.resetAllMocks();
    mockUseSelector.mockImplementation((selector) => selector());
  });

  it('should render win32controls-container wrapper', () => {
    const wrapper = shallow(<Win32Controls />);

    expect(wrapper.find('.win32controls-container')).toHaveLength(1);
  });

  it('should always render MinButton and CloseButton', () => {
    const wrapper = shallow(<Win32Controls />);

    expect(wrapper.find(MockMinButton)).toHaveLength(1);
    expect(wrapper.find(MockCloseButton)).toHaveLength(1);
  });

  it('should render UnmaxButton if screen is maximized', () => {
    mockGetIsScreenMaximized.mockReturnValue(true);
    const wrapper = shallow(<Win32Controls />);

    expect(wrapper.find(MockUnmaxButton)).toHaveLength(1);
  });

  it('should not render MaxButton if screen is maximized', () => {
    mockGetIsScreenMaximized.mockReturnValue(true);
    const wrapper = shallow(<Win32Controls />);

    expect(wrapper.find(MockMaxButton)).toHaveLength(0);
  });

  it('should render MaxButton if screen is unmaximized', () => {
    mockGetIsScreenMaximized.mockReturnValue(false);
    const wrapper = shallow(<Win32Controls />);

    expect(wrapper.find(MockMaxButton)).toHaveLength(1);
  });

  it('should not render UnmaxButton if screen is unmaximized', () => {
    mockGetIsScreenMaximized.mockReturnValue(false);
    const wrapper = shallow(<Win32Controls />);

    expect(wrapper.find(MockUnmaxButton)).toHaveLength(0);
  });
});
