import React from 'react';
import { shallow } from 'enzyme';
import Toolbar from '../../../../src/components/Toolbar/Toolbar';

// eslint-disable-next-line @typescript-eslint/no-explicit-any
let MockWin32Controls: any;
jest.mock(
  '../../../../src/components/Toolbar/Win32Controls/Win32Controls',
  () => {
    // eslint-disable-next-line react/display-name
    MockWin32Controls = () => <></>;
    return {
      __esModule: true,
      default: MockWin32Controls
    };
  }
);

let mockUseSelector: jest.Mock;
jest.mock('react-redux', () => {
  mockUseSelector = jest.fn();
  return {
    useSelector: mockUseSelector
  };
});

let mockGetPlatform: jest.Mock;
jest.mock('../../../../src/common/redux/mainSlice', () => {
  mockGetPlatform = jest.fn();
  return {
    getPlatform: mockGetPlatform
  };
});

describe('Toolbar', () => {
  beforeEach(() => {
    jest.resetAllMocks();
    mockUseSelector.mockImplementation((selector) => selector());
  });

  it('should render toolbar-wrapper div', () => {
    const wrapper = shallow(<Toolbar />);

    expect(wrapper.find('.toolbar-wrapper')).toHaveLength(1);
  });

  it('should return Win32Controls if platform is win32', () => {
    mockGetPlatform.mockReturnValue('win32');

    const wrapper = shallow(<Toolbar />);

    expect(wrapper.find(MockWin32Controls)).toHaveLength(1);
  });

  it('should not return Win32Controls if platform is darwin', () => {
    mockGetPlatform.mockReturnValue('darwin');

    const wrapper = shallow(<Toolbar />);

    expect(wrapper.find(MockWin32Controls)).toHaveLength(0);
  });
});
