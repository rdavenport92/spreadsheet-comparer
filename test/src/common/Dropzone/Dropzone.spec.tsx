import React from 'react';
import Dropzone from '../../../../src/common/Dropzone/Dropzone';
import { shallow, render } from 'enzyme';

const mockOnClick = jest.fn();
let mockUseDropzone: jest.Mock;
const mockGetInputProps = jest.fn();
const mockGetRootProps = jest.fn();
jest.mock('react-dropzone', () => {
  mockUseDropzone = jest.fn();
  return {
    useDropzone: mockUseDropzone
  };
});

// eslint-disable-next-line @typescript-eslint/no-explicit-any
let MockFileIcon: any;
jest.mock('react-feather', () => {
  // eslint-disable-next-line react/display-name
  MockFileIcon = () => <></>;
  return {
    File: MockFileIcon
  };
});

// eslint-disable-next-line @typescript-eslint/no-explicit-any
let MockChooseFilesButton: any;
jest.mock(
  '../../../../src/common/buttons/ChooseFilesButton/ChooseFilesButton',
  () => {
    // eslint-disable-next-line react/display-name
    MockChooseFilesButton = () => <></>;
    return {
      __esModule: true,
      default: MockChooseFilesButton
    };
  }
);

// eslint-disable-next-line @typescript-eslint/no-explicit-any
let MockCloseButton: any;
jest.mock('../../../../src/common/buttons/CloseButton/CloseButton', () => {
  // eslint-disable-next-line react/display-name
  MockCloseButton = () => <></>;
  return {
    __esModule: true,
    default: MockCloseButton
  };
});

describe('Dropzone', () => {
  beforeEach(() => {
    jest.resetAllMocks();
    mockUseDropzone.mockImplementation(() => ({
      getRootProps: mockGetRootProps,
      getInputProps: mockGetInputProps
    }));
    mockGetRootProps.mockImplementation((config) => ({
      ...config,
      onClick: mockOnClick
    }));
    mockGetInputProps.mockImplementation(() => ({ testprop: 'testvalue' }));
  });

  it('should render div with className dropzone', () => {
    const wrapper = shallow(<Dropzone onDrop={jest.fn()} />);

    expect(wrapper.find('.dropzone')).toHaveLength(1);
  });

  it('should render file icon', () => {
    const wrapper = shallow(<Dropzone onDrop={jest.fn()} />);

    expect(wrapper.find(MockFileIcon)).toHaveLength(1);
  });

  it('should render Choose Files button', () => {
    const wrapper = shallow(<Dropzone onDrop={jest.fn()} />);
    const button = wrapper.find(MockChooseFilesButton);
    expect(button).toHaveLength(1);
  });

  it('should render text "or drop file here"', () => {
    const wrapper = render(<Dropzone onDrop={jest.fn()} />);

    expect(wrapper.find('.dropzone-text').text()).toEqual('or drop file here');
  });

  it('should render CloseButton if closable prop is true', () => {
    const wrapper = shallow(<Dropzone onDrop={jest.fn()} closeable={true} />);

    expect(wrapper.find(MockCloseButton)).toHaveLength(1);
  });

  it('should not render CloseButton if closable prop is falsey', () => {
    const wrapper = shallow(<Dropzone onDrop={jest.fn()} />);

    expect(wrapper.find(MockCloseButton)).toHaveLength(0);
  });

  it('invokes onClose prop when CloseButton is clicked', () => {
    const mockOnClose = jest.fn();
    const wrapper = shallow(
      <Dropzone closeable={true} onDrop={jest.fn()} onClose={mockOnClose} />
    );

    wrapper.find(MockCloseButton).simulate('click');

    expect(mockOnClose).toHaveBeenCalled();
  });

  it('invokes console.error if onClose function not provided but closeable', () => {
    const errorSpy = jest.spyOn(console, 'error').mockImplementation(() => {
      return;
    });
    const wrapper = shallow(<Dropzone onDrop={jest.fn()} closeable={true} />);

    wrapper.find(MockCloseButton).simulate('click');

    expect(errorSpy).toHaveBeenCalledWith('No onClose function was provided');
  });

  it('invoke useDropzone with passed in onDrop handler', () => {
    const mockOnDrop = jest.fn();
    shallow(<Dropzone onDrop={mockOnDrop} />);

    expect(mockUseDropzone).toHaveBeenCalledWith(
      expect.objectContaining({ onDrop: mockOnDrop })
    );
  });

  it('should render input with InputProps', () => {
    const wrapper = shallow(<Dropzone onDrop={jest.fn()} />);

    expect(
      wrapper.find('[data-testid="dropzone-input"]').prop('testprop')
    ).toEqual('testvalue');
  });

  it('should remove onClick from rootProps', () => {
    const wrapper = shallow(<Dropzone onDrop={jest.fn()} />);

    expect(
      wrapper.find('[data-testid="dropzone-wrapper"]').prop('onClick')
    ).toBeUndefined();
  });

  it('should assign onClick from getRootProps to ChooseFilesButton onClick handler', () => {
    const wrapper = shallow(<Dropzone onDrop={jest.fn()} />);

    expect(wrapper.find(MockChooseFilesButton).prop('onClick')).toBe(
      mockOnClick
    );
  });
});
