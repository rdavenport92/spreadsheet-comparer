import React from 'react';
import Hr from '../../../../src/common/Hr/Hr';
import { shallow } from 'enzyme';

describe('Hr', () => {
  beforeEach(() => {
    jest.resetAllMocks();
  });

  it('should render an hr element with class hr', () => {
    const wrapper = shallow(<Hr />);

    const hr = wrapper.find('[data-testid="hr"]');
    expect(hr.type()).toEqual('hr');
    expect(hr.hasClass('hr')).toBe(true);
  });
});
