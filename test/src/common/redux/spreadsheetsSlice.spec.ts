import {
  getChildrenSpreadsheets,
  getMasterSpreadsheet,
  initialState,
  reducers,
  setupSlice,
  spreadsheetsSlice
} from '../../../../src/common/redux/spreadsheetsSlice';
import { RootState } from '../../../../src/store';

const { setMasterSpreadsheet, addChildSpreadsheet } = reducers;

let mockCreateSlice: jest.Mock;
jest.mock('@reduxjs/toolkit', () => {
  mockCreateSlice = jest.fn().mockReturnValue({
    reducer: {},
    actions: { setMasterSpreadsheet, addChildSpreadsheet }
  });
  return {
    createSlice: mockCreateSlice
  };
});

describe('spreadsheetsSlice', () => {
  beforeEach(() => {
    jest.resetAllMocks();
  });

  it('should call createSlice with appropriate config object', () => {
    setupSlice();
    expect(mockCreateSlice).toHaveBeenCalledWith(
      expect.objectContaining({
        name: 'spreadsheets',
        initialState,
        reducers
      })
    );
  });

  it('should create mainSlice with reducer property', () => {
    expect(spreadsheetsSlice).toEqual(expect.objectContaining({ reducer: {} }));
  });

  describe('reducers', () => {
    describe('setMasterSpreadsheet', () => {
      it('should set master spreadsheet with filename', () => {
        const result = setMasterSpreadsheet(initialState, {
          payload: 'test-filename'
        });
        expect(result.master).toEqual(
          expect.objectContaining({ filename: 'test-filename' })
        );
      });
    });

    describe('addChildSpreadsheet', () => {
      it('should add a spreadsheet to child array', () => {
        const result = addChildSpreadsheet(initialState, {
          payload: 'test-filename'
        });
        expect(result.children).toHaveLength(1);
        expect(result.children[0]).toEqual(
          expect.objectContaining({ filename: 'test-filename' })
        );
      });
    });
  });

  describe('selectors', () => {
    describe('getMasterSpreadsheet', () => {
      it('should return master spreadsheet', () => {
        const master = Symbol('masterSpreadsheet');
        const testState = ({
          spreadsheets: { master }
        } as unknown) as RootState;

        expect(getMasterSpreadsheet(testState)).toEqual(master);
      });
    });

    describe('getChildrenSpreadsheets', () => {
      it('should return children spreadsheets', () => {
        const children = Symbol('children');
        const testState = ({
          spreadsheets: { children }
        } as unknown) as RootState;

        expect(getChildrenSpreadsheets(testState)).toEqual(children);
      });
    });
  });
});
