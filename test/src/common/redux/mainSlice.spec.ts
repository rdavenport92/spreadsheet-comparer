import {
  mainSlice,
  setupSlice,
  initialState,
  getPlatform,
  getIsScreenMaximized,
  Platform,
  reducers
} from '../../../../src/common/redux/mainSlice';
import { RootState } from '../../../../src/store';

const { setPlatform, setScreenMaxStatus } = reducers;

let mockCreateSlice: jest.Mock;
jest.mock('@reduxjs/toolkit', () => {
  mockCreateSlice = jest.fn().mockReturnValue({
    reducer: {},
    actions: { setPlatform, setScreenMaxStatus }
  });
  return {
    createSlice: mockCreateSlice
  };
});

describe('mainSlice', () => {
  beforeEach(() => {
    jest.resetAllMocks();
  });

  it('should call createSlice with appropriate config object', () => {
    setupSlice();
    expect(mockCreateSlice).toHaveBeenCalledWith(
      expect.objectContaining({
        name: 'main',
        initialState,
        reducers
      })
    );
  });

  it('should create mainSlice with reducer property', () => {
    expect(mainSlice).toEqual(expect.objectContaining({ reducer: {} }));
  });

  describe('reducers', () => {
    describe('setPlatform', () => {
      it('should update platform and return new state', () => {
        const result = setPlatform(initialState, {
          payload: Platform.WIN32,
          type: 'main/setPlatform'
        });
        expect(result).toEqual(
          expect.objectContaining({ ...initialState, platform: Platform.WIN32 })
        );
      });
    });

    describe('setScreenMaxStatus', () => {
      it('should update isScreenMaximized and return new state', () => {
        const result = setScreenMaxStatus(initialState, {
          payload: true,
          type: 'main/isScreenMaximized'
        });
        expect(result).toEqual(
          expect.objectContaining({ ...initialState, isScreenMaximized: true })
        );
      });
    });
  });

  describe('selectors', () => {
    describe('getPlatform', () => {
      it('should return platform', () => {
        const platform = Symbol('platform');
        const testState = ({ main: { platform } } as unknown) as RootState;

        expect(getPlatform(testState)).toEqual(platform);
      });
    });

    describe('getIsScreenMaximized', () => {
      it('should return isScreenMaximized', () => {
        const isScreenMaximized = Symbol('isScreenMaximized');
        const testState = ({
          main: { isScreenMaximized }
        } as unknown) as RootState;

        expect(getIsScreenMaximized(testState)).toEqual(isScreenMaximized);
      });
    });
  });
});
