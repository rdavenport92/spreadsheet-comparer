import React from 'react';
import ExecuteComparisonButton from '../../../../../src/common/buttons/ExecuteComparisonButton/ExecuteComparisonButton';
import { ButtonType } from '../../../../../src/common/buttons/ButtonWrapper';
import { shallow } from 'enzyme';

// eslint-disable-next-line @typescript-eslint/no-explicit-any
let MockButtonWrapper: any;
jest.mock('../../../../../src/common/buttons/ButtonWrapper', () => {
  // eslint-disable-next-line react/display-name
  MockButtonWrapper = (type: ButtonType, label: string | null) => (
    <>{type && label}</>
  );
  return {
    ...(jest.requireActual(
      '../../../../../src/common/buttons/ButtonWrapper'
      // eslint-disable-next-line @typescript-eslint/no-explicit-any
    ) as any),
    __esModule: true,
    default: MockButtonWrapper
  };
});

// eslint-disable-next-line @typescript-eslint/no-explicit-any
let MockCornerDownLeftIcon: any;
jest.mock('react-feather', () => {
  // eslint-disable-next-line react/display-name
  MockCornerDownLeftIcon = () => <></>;
  return {
    CornerDownLeft: MockCornerDownLeftIcon
  };
});

describe('ExecuteComparisonButton', () => {
  beforeEach(() => {
    jest.resetAllMocks();
  });

  it('should render ButtonWrapper with prop type set to PRIMARY', () => {
    const wrapper = shallow(<ExecuteComparisonButton />);

    const ButtonWrapper = wrapper.find(MockButtonWrapper);
    // eslint-disable-next-line @typescript-eslint/no-explicit-any
    expect((ButtonWrapper.props() as any).type).toEqual(ButtonType.PRIMARY);
  });

  it('should render label as "Compare" if includeLabel is set to true', () => {
    const wrapper = shallow(<ExecuteComparisonButton includeLabel={true} />);

    const ButtonWrapper = wrapper.find(MockButtonWrapper);
    // eslint-disable-next-line @typescript-eslint/no-explicit-any
    expect((ButtonWrapper.props() as any).label).toEqual('Compare');
  });

  it('should render label as null if includeLabel is set to false', () => {
    const wrapper = shallow(<ExecuteComparisonButton includeLabel={false} />);

    const ButtonWrapper = wrapper.find(MockButtonWrapper);
    // eslint-disable-next-line @typescript-eslint/no-explicit-any
    expect((ButtonWrapper.props() as any).label).toBeNull();
  });

  it('should render CornerDownLeft icon', () => {
    const wrapper = shallow(<ExecuteComparisonButton />);

    expect(wrapper.find(MockCornerDownLeftIcon)).toHaveLength(1);
  });

  it('should pass strong prop set to true into ButtonWrapper if includeLabel is true', () => {
    const wrapper = shallow(<ExecuteComparisonButton includeLabel={true} />);

    const ButtonWrapper = wrapper.find(MockButtonWrapper);
    // eslint-disable-next-line @typescript-eslint/no-explicit-any
    expect((ButtonWrapper.props() as any).strong).toBe(true);
  });

  it('should pass strong prop set to false into ButtonWrapper if includeLabel is falsey', () => {
    const wrapper = shallow(<ExecuteComparisonButton />);

    const ButtonWrapper = wrapper.find(MockButtonWrapper);
    // eslint-disable-next-line @typescript-eslint/no-explicit-any
    expect((ButtonWrapper.props() as any).strong).toBe(false);
  });
});
