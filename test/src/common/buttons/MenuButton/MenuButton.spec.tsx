import React from 'react';
import MenuButton from '../../../../../src/common/buttons/MenuButton/MenuButton';
import { ButtonType } from '../../../../../src/common/buttons/ButtonWrapper';
import { shallow } from 'enzyme';

// eslint-disable-next-line @typescript-eslint/no-explicit-any
let MockButtonWrapper: any;
jest.mock('../../../../../src/common/buttons/ButtonWrapper', () => {
  // eslint-disable-next-line react/display-name
  MockButtonWrapper = (type: ButtonType) => <>{type}</>;
  return {
    ...(jest.requireActual(
      '../../../../../src/common/buttons/ButtonWrapper'
      // eslint-disable-next-line @typescript-eslint/no-explicit-any
    ) as any),
    __esModule: true,
    default: MockButtonWrapper
  };
});

// eslint-disable-next-line @typescript-eslint/no-explicit-any
let MockMenuIcon: any;
jest.mock('react-feather', () => {
  // eslint-disable-next-line react/display-name
  MockMenuIcon = () => <></>;
  return {
    Menu: MockMenuIcon
  };
});

describe('MenuButton', () => {
  beforeEach(() => {
    jest.resetAllMocks();
  });

  it('should render ButtonWrapper with prop type set to SECONDARY', () => {
    const wrapper = shallow(<MenuButton />);

    const ButtonWrapper = wrapper.find(MockButtonWrapper);
    // eslint-disable-next-line @typescript-eslint/no-explicit-any
    expect((ButtonWrapper.props() as any).type).toEqual(ButtonType.SECONDARY);
  });

  it('should render Menu icon', () => {
    const wrapper = shallow(<MenuButton />);

    expect(wrapper.find(MockMenuIcon)).toHaveLength(1);
  });
});
