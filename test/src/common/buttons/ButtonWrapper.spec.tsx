import React from 'react';
import ButtonWrapper, {
  ButtonType
} from '../../../../src/common/buttons/ButtonWrapper';
import { shallow } from 'enzyme';

// eslint-disable-next-line @typescript-eslint/no-explicit-any
let MockButton: any;
jest.mock('@material/react-button', () => {
  // eslint-disable-next-line react/display-name
  MockButton = (className: string) => <div className={className}></div>;
  return {
    __esModule: true,
    default: MockButton
  };
});

describe('ButtonWrapper', () => {
  beforeEach(() => {
    jest.resetAllMocks();
  });

  it('should pass button-wrapper-primary className into Button component if type is PRIMARY', () => {
    const wrapper = shallow(
      <ButtonWrapper onClick={jest.fn()} type={ButtonType.PRIMARY} />
    );
    const buttonComponent = wrapper.find(MockButton);

    expect(buttonComponent.hasClass('button-wrapper-primary')).toBe(true);
  });

  it('should pass button-wrapper-secondary className into Button component if type is SECONDARY', () => {
    const wrapper = shallow(
      <ButtonWrapper onClick={jest.fn()} type={ButtonType.SECONDARY} />
    );
    const buttonComponent = wrapper.find(MockButton);

    expect(buttonComponent.hasClass('button-wrapper-secondary')).toBe(true);
  });

  it('should pass button-wrapper-transparent className into Button component if type is TRANSPARENT', () => {
    const wrapper = shallow(
      <ButtonWrapper onClick={jest.fn()} type={ButtonType.TRANSPARENT} />
    );
    const buttonComponent = wrapper.find(MockButton);

    expect(buttonComponent.hasClass('button-wrapper-transparent')).toBe(true);
  });

  it('should not pass button-wrapper-primary className into Button component if type is SECONDARY', () => {
    const wrapper = shallow(
      <ButtonWrapper onClick={jest.fn()} type={ButtonType.SECONDARY} />
    );
    const buttonComponent = wrapper.find(MockButton);

    expect(buttonComponent.hasClass('button-wrapper-primary')).toBe(false);
  });

  it('should not pass button-wrapper-transparent className into Button component if type is SECONDARY', () => {
    const wrapper = shallow(
      <ButtonWrapper onClick={jest.fn()} type={ButtonType.SECONDARY} />
    );
    const buttonComponent = wrapper.find(MockButton);

    expect(buttonComponent.hasClass('button-wrapper-transparent')).toBe(false);
  });

  it('should not pass button-wrapper-secondary className into Button component if type is PRIMARY', () => {
    const wrapper = shallow(
      <ButtonWrapper onClick={jest.fn()} type={ButtonType.PRIMARY} />
    );
    const buttonComponent = wrapper.find(MockButton);

    expect(buttonComponent.hasClass('button-wrapper-secondary')).toBe(false);
  });

  it('should not pass button-wrapper-transparent className into Button component if type is PRIMARY', () => {
    const wrapper = shallow(
      <ButtonWrapper onClick={jest.fn()} type={ButtonType.PRIMARY} />
    );
    const buttonComponent = wrapper.find(MockButton);

    expect(buttonComponent.hasClass('button-wrapper-transparent')).toBe(false);
  });

  it('should not pass button-wrapper-primary className into Button component if type is TRANSPARENT', () => {
    const wrapper = shallow(
      <ButtonWrapper onClick={jest.fn()} type={ButtonType.TRANSPARENT} />
    );
    const buttonComponent = wrapper.find(MockButton);

    expect(buttonComponent.hasClass('button-wrapper-primary')).toBe(false);
  });

  it('should not pass button-wrapper-secondary className into Button component if type is TRANSPARENT', () => {
    const wrapper = shallow(
      <ButtonWrapper onClick={jest.fn()} type={ButtonType.TRANSPARENT} />
    );
    const buttonComponent = wrapper.find(MockButton);

    expect(buttonComponent.hasClass('button-wrapper-secondary')).toBe(false);
  });

  it('should render children if passed in', () => {
    const wrapper = shallow(
      <ButtonWrapper onClick={jest.fn()} type={ButtonType.PRIMARY}>
        <div className="test" />
      </ButtonWrapper>
    );

    expect(wrapper.find('.test')).toHaveLength(1);
  });

  it('should render label if passed in', () => {
    const wrapper = shallow(
      <ButtonWrapper
        onClick={jest.fn()}
        type={ButtonType.PRIMARY}
        label="test"
      />
    );
    const labelContainer = wrapper.find('.button-label');

    expect(labelContainer.text()).toEqual('test');
  });

  it('should render spacer if children and label are passed in', () => {
    const wrapper = shallow(
      <ButtonWrapper onClick={jest.fn()} type={ButtonType.PRIMARY} label="test">
        <div>test</div>
      </ButtonWrapper>
    );

    expect(wrapper.find('.spacer')).toHaveLength(1);
  });

  it('should not render spacer if children are passed in with no label prop', () => {
    const wrapper = shallow(
      <ButtonWrapper onClick={jest.fn()} type={ButtonType.PRIMARY}>
        <div>test</div>
      </ButtonWrapper>
    );

    expect(wrapper.find('.spacer')).toHaveLength(0);
  });

  it('should not render spacer if no children are passed in with label prop', () => {
    const wrapper = shallow(
      <ButtonWrapper
        onClick={jest.fn()}
        type={ButtonType.PRIMARY}
        label="test"
      />
    );

    expect(wrapper.find('.spacer')).toHaveLength(0);
  });

  it('should have button-wrapper-stretch class if strong prop true', () => {
    const wrapper = shallow(
      <ButtonWrapper
        strong={true}
        onClick={jest.fn()}
        type={ButtonType.PRIMARY}
      />
    );

    const Button = wrapper.find(MockButton);
    expect(Button.hasClass('button-wrapper-stretch')).toBe(true);
  });

  it('should not have button-wrapper-stretch class if strong prop falsey', () => {
    const wrapper = shallow(
      <ButtonWrapper onClick={jest.fn()} type={ButtonType.PRIMARY} />
    );

    const Button = wrapper.find(MockButton);
    expect(Button.hasClass('button-wrapper-stretch')).toBe(false);
  });

  it('should render label with textTransform uppercase and fontWeight bold if strong prop true', () => {
    const wrapper = shallow(
      <ButtonWrapper
        strong={true}
        onClick={jest.fn()}
        type={ButtonType.PRIMARY}
      />
    );

    // eslint-disable-next-line @typescript-eslint/no-explicit-any
    expect((wrapper.find('.button-label').props() as any).style).toEqual(
      expect.objectContaining({
        textTransform: 'uppercase',
        fontWeight: 'bold'
      })
    );
  });

  it('should not render label with textTransform uppercase and fontWeight bold if strong prop falsey', () => {
    const wrapper = shallow(
      <ButtonWrapper onClick={jest.fn()} type={ButtonType.PRIMARY} />
    );

    // eslint-disable-next-line @typescript-eslint/no-explicit-any
    expect((wrapper.find('.button-label').props() as any).style).toEqual({});
  });

  it('should invoke onClick prop when Button is clicked', () => {
    const mockOnClick = jest.fn();
    const wrapper = shallow(
      <ButtonWrapper onClick={mockOnClick} type={ButtonType.PRIMARY} />
    );

    wrapper.find(MockButton).simulate('click');

    expect(mockOnClick).toHaveBeenCalled();
  });
});
