import React from 'react';
import Win32MinButton from '../../../../../../src/common/buttons/win32/Win32MinButton/Win32MinButton';
import { Win32ButtonType } from '../../../../../../src/common/buttons/win32/Win32ButtonWrapper';
import { shallow } from 'enzyme';
// eslint-disable-next-line @typescript-eslint/no-explicit-any
let MockMinus: any;
jest.mock('react-feather', () => {
  // eslint-disable-next-line react/display-name
  MockMinus = () => <></>;
  return {
    Minus: MockMinus
  };
});

// eslint-disable-next-line @typescript-eslint/no-explicit-any
let MockWin32ButtonWrapper: any;
jest.mock(
  '../../../../../../src/common/buttons/win32/Win32ButtonWrapper',
  () => {
    // eslint-disable-next-line react/display-name
    MockWin32ButtonWrapper = () => <></>;
    return {
      ...(jest.requireActual(
        '../../../../../../src/common/buttons/win32/Win32ButtonWrapper'
        // eslint-disable-next-line @typescript-eslint/no-explicit-any
      ) as any),
      __esModule: true,
      default: MockWin32ButtonWrapper
    };
  }
);

describe('Win32MaxButton', () => {
  beforeEach(() => {
    jest.resetAllMocks();
  });

  it('should be wrapped in Win32ButtonWrapper with MIN as type prop', () => {
    const wrapper = shallow(<Win32MinButton />);

    const buttonWrapper = wrapper.find(MockWin32ButtonWrapper);
    expect(buttonWrapper).toHaveLength(1);
    // eslint-disable-next-line @typescript-eslint/no-explicit-any
    expect((buttonWrapper.props() as any).type).toEqual(Win32ButtonType.MIN);
  });

  it('should render win32-min-btn-wrapper', () => {
    const wrapper = shallow(<Win32MinButton />);

    expect(wrapper.find('.win32-min-btn-wrapper')).toHaveLength(1);
  });

  it('should render Minus icon', () => {
    const wrapper = shallow(<Win32MinButton />);

    expect(wrapper.find(MockMinus)).toHaveLength(1);
  });
});
