import React from 'react';
import { shallow } from 'enzyme';
import Win32ButtonWrapper, {
  Win32ButtonType
} from '../../../../../src/common/buttons/win32/Win32ButtonWrapper';

let ElectronService: () => void;
let mockCloseWindow: jest.Mock;
let mockMaximizeWindow: jest.Mock;
let mockUnmaximizeWindow: jest.Mock;
let mockMinimizeWindow: jest.Mock;
jest.mock('../../../../../src/shared/ElectronService/ElectronService', () => {
  mockCloseWindow = jest.fn();
  mockMaximizeWindow = jest.fn();
  mockUnmaximizeWindow = jest.fn();
  mockMinimizeWindow = jest.fn();
  // eslint-disable-next-line @typescript-eslint/no-explicit-any
  ElectronService = function (this: any) {
    this.closeWindow = mockCloseWindow;
    this.maximizeWindow = mockMaximizeWindow;
    this.unmaximizeWindow = mockUnmaximizeWindow;
    this.minimizeWindow = mockMinimizeWindow;
  };
  return {
    __esModule: true,
    default: ElectronService
  };
});

describe('Win32ButtonWrapper', () => {
  beforeEach(() => {
    jest.resetAllMocks();
  });

  it('should render win32-btn-wrapper', () => {
    const wrapper = shallow(
      <Win32ButtonWrapper type={Win32ButtonType.CLOSE} />
    );

    expect(wrapper.find('.win32-btn-wrapper')).toHaveLength(1);
  });

  it('should render children that are passed in as prop', () => {
    const wrapper = shallow(
      <Win32ButtonWrapper type={Win32ButtonType.CLOSE}>
        <div className="test-child" />
      </Win32ButtonWrapper>
    );

    expect(wrapper.find('.test-child')).toHaveLength(1);
  });

  describe('clickHandler', () => {
    it('should call electronService.closeWindow onClick if prop type is close', () => {
      const wrapper = shallow(
        <Win32ButtonWrapper type={Win32ButtonType.CLOSE} />
      );

      wrapper.find('.win32-btn-wrapper').simulate('click');

      expect(mockCloseWindow).toHaveBeenCalled();
    });

    it('should call electronService.maximizeWindow onClick if prop type is max', () => {
      const wrapper = shallow(
        <Win32ButtonWrapper type={Win32ButtonType.MAX} />
      );

      wrapper.find('.win32-btn-wrapper').simulate('click');

      expect(mockMaximizeWindow).toHaveBeenCalled();
    });

    it('should call electronService.unmaximizeWindow onClick if prop type is unmax', () => {
      const wrapper = shallow(
        <Win32ButtonWrapper type={Win32ButtonType.UNMAX} />
      );

      wrapper.find('.win32-btn-wrapper').simulate('click');

      expect(mockUnmaximizeWindow).toHaveBeenCalled();
    });

    it('should call electronService.minimizeWindow onClick if prop type is min', () => {
      const wrapper = shallow(
        <Win32ButtonWrapper type={Win32ButtonType.MIN} />
      );

      wrapper.find('.win32-btn-wrapper').simulate('click');

      expect(mockMinimizeWindow).toHaveBeenCalled();
    });
  });
});
