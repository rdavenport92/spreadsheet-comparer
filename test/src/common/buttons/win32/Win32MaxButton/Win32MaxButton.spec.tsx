import React from 'react';
import Win32MaxButton from '../../../../../../src/common/buttons/win32/Win32MaxButton/Win32MaxButton';
import { Win32ButtonType } from '../../../../../../src/common/buttons/win32/Win32ButtonWrapper';
import { shallow } from 'enzyme';
// eslint-disable-next-line @typescript-eslint/no-explicit-any
let MockSquare: any;
jest.mock('react-feather', () => {
  // eslint-disable-next-line react/display-name
  MockSquare = () => <></>;
  return {
    Square: MockSquare
  };
});

// eslint-disable-next-line @typescript-eslint/no-explicit-any
let MockWin32ButtonWrapper: any;
jest.mock(
  '../../../../../../src/common/buttons/win32/Win32ButtonWrapper',
  () => {
    // eslint-disable-next-line react/display-name
    MockWin32ButtonWrapper = () => <></>;
    return {
      ...(jest.requireActual(
        '../../../../../../src/common/buttons/win32/Win32ButtonWrapper'
        // eslint-disable-next-line @typescript-eslint/no-explicit-any
      ) as any),
      __esModule: true,
      default: MockWin32ButtonWrapper
    };
  }
);

describe('Win32MaxButton', () => {
  beforeEach(() => {
    jest.resetAllMocks();
  });

  it('should be wrapped in Win32ButtonWrapper with MAX as type prop', () => {
    const wrapper = shallow(<Win32MaxButton />);

    const buttonWrapper = wrapper.find(MockWin32ButtonWrapper);
    expect(buttonWrapper).toHaveLength(1);
    // eslint-disable-next-line @typescript-eslint/no-explicit-any
    expect((buttonWrapper.props() as any).type).toEqual(Win32ButtonType.MAX);
  });

  it('should render win32-max-btn-wrapper', () => {
    const wrapper = shallow(<Win32MaxButton />);

    expect(wrapper.find('.win32-max-btn-wrapper')).toHaveLength(1);
  });

  it('should render square-wrapper', () => {
    const wrapper = shallow(<Win32MaxButton />);

    expect(wrapper.find('.square-wrapper')).toHaveLength(1);
  });

  it('should render Square icon', () => {
    const wrapper = shallow(<Win32MaxButton />);

    expect(wrapper.find(MockSquare)).toHaveLength(1);
  });
});
