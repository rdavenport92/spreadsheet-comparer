import React from 'react';
import Win32CloseButton from '../../../../../../src/common/buttons/win32/Win32CloseButton/Win32CloseButton';
import { Win32ButtonType } from '../../../../../../src/common/buttons/win32/Win32ButtonWrapper';
import { shallow } from 'enzyme';
// eslint-disable-next-line @typescript-eslint/no-explicit-any
let MockX: any;
jest.mock('react-feather', () => {
  // eslint-disable-next-line react/display-name
  MockX = () => <></>;
  return {
    X: MockX
  };
});

// eslint-disable-next-line @typescript-eslint/no-explicit-any
let MockWin32ButtonWrapper: any;
jest.mock(
  '../../../../../../src/common/buttons/win32/Win32ButtonWrapper',
  () => {
    // eslint-disable-next-line react/display-name
    MockWin32ButtonWrapper = () => <></>;
    return {
      ...(jest.requireActual(
        '../../../../../../src/common/buttons/win32/Win32ButtonWrapper'
        // eslint-disable-next-line @typescript-eslint/no-explicit-any
      ) as any),
      __esModule: true,
      default: MockWin32ButtonWrapper
    };
  }
);

describe('Win32CloseButton', () => {
  beforeEach(() => {
    jest.resetAllMocks();
  });

  it('should be wrapped in Win32ButtonWrapper with CLOSE as type prop', () => {
    const wrapper = shallow(<Win32CloseButton />);

    const buttonWrapper = wrapper.find(MockWin32ButtonWrapper);
    expect(buttonWrapper).toHaveLength(1);
    // eslint-disable-next-line @typescript-eslint/no-explicit-any
    expect((buttonWrapper.props() as any).type).toEqual(Win32ButtonType.CLOSE);
  });

  it('should render win32-close-btn-wrapper', () => {
    const wrapper = shallow(<Win32CloseButton />);

    expect(wrapper.find('.win32-close-btn-wrapper')).toHaveLength(1);
  });

  it('should render X icon', () => {
    const wrapper = shallow(<Win32CloseButton />);

    expect(wrapper.find(MockX)).toHaveLength(1);
  });
});
