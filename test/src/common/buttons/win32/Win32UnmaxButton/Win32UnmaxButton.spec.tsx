import React from 'react';
import Win32UnmaxButton from '../../../../../../src/common/buttons/win32/Win32UnmaxButton/Win32UnmaxButton';
import { Win32ButtonType } from '../../../../../../src/common/buttons/win32/Win32ButtonWrapper';
import { shallow } from 'enzyme';
// eslint-disable-next-line @typescript-eslint/no-explicit-any
let MockSquare: any;
jest.mock('react-feather', () => {
  // eslint-disable-next-line react/display-name
  MockSquare = () => <></>;
  return {
    Square: MockSquare
  };
});

// eslint-disable-next-line @typescript-eslint/no-explicit-any
let MockWin32ButtonWrapper: any;
jest.mock(
  '../../../../../../src/common/buttons/win32/Win32ButtonWrapper',
  () => {
    // eslint-disable-next-line react/display-name
    MockWin32ButtonWrapper = () => <></>;
    return {
      ...(jest.requireActual(
        '../../../../../../src/common/buttons/win32/Win32ButtonWrapper'
        // eslint-disable-next-line @typescript-eslint/no-explicit-any
      ) as any),
      __esModule: true,
      default: MockWin32ButtonWrapper
    };
  }
);

describe('Win32UnmaxButton', () => {
  beforeEach(() => {
    jest.resetAllMocks();
  });

  it('should be wrapped in Win32ButtonWrapper with UNMAX as type prop', () => {
    const wrapper = shallow(<Win32UnmaxButton />);

    const buttonWrapper = wrapper.find(MockWin32ButtonWrapper);
    expect(buttonWrapper).toHaveLength(1);
    // eslint-disable-next-line @typescript-eslint/no-explicit-any
    expect((buttonWrapper.props() as any).type).toEqual(Win32ButtonType.UNMAX);
  });

  it('should render win32-unmax-btn-wrapper', () => {
    const wrapper = shallow(<Win32UnmaxButton />);

    expect(wrapper.find('.win32-unmax-btn-wrapper')).toHaveLength(1);
  });

  it('should render square-1 and square-2 wrappers', () => {
    const wrapper = shallow(<Win32UnmaxButton />);

    expect(wrapper.find('.win32-unmax-square-1')).toHaveLength(1);
    expect(wrapper.find('.win32-unmax-square-2')).toHaveLength(1);
  });

  it('should render 2 Square icons', () => {
    const wrapper = shallow(<Win32UnmaxButton />);

    expect(wrapper.find(MockSquare)).toHaveLength(2);
  });
});
