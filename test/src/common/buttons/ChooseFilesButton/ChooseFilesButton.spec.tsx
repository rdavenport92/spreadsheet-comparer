import { shallow } from 'enzyme';
import React from 'react';

import { ButtonType } from '../../../../../src/common/buttons/ButtonWrapper';
import ChooseFilesButton from '../../../../../src/common/buttons/ChooseFilesButton/ChooseFilesButton';

// eslint-disable-next-line @typescript-eslint/no-explicit-any
let MockButtonWrapper: any;
jest.mock('../../../../../src/common/buttons/ButtonWrapper', () => {
  // eslint-disable-next-line react/display-name
  MockButtonWrapper = (type: ButtonType, label: string | null) => (
    <>{type && label}</>
  );
  return {
    ...(jest.requireActual(
      '../../../../../src/common/buttons/ButtonWrapper'
      // eslint-disable-next-line @typescript-eslint/no-explicit-any
    ) as any),
    __esModule: true,
    default: MockButtonWrapper
  };
});

describe('ChooseFilesButton', () => {
  beforeEach(() => {
    jest.resetAllMocks();
  });

  it('should render ButtonWrapper with prop type PRIMARY', () => {
    const wrapper = shallow(<ChooseFilesButton onClick={jest.fn()} />);

    const button = wrapper.find(MockButtonWrapper);
    expect(button).toHaveLength(1);
    // eslint-disable-next-line @typescript-eslint/no-explicit-any
    expect((button.props() as any).type).toEqual(ButtonType.PRIMARY);
  });

  it('should set label on ButtonWrapper to Choose Files', () => {
    const wrapper = shallow(<ChooseFilesButton onClick={jest.fn()} />);

    const button = wrapper.find(MockButtonWrapper);
    // eslint-disable-next-line @typescript-eslint/no-explicit-any
    expect((button.props() as any).label).toEqual('Choose Files');
  });

  it('should pass onClick prop to ButtonWrapper', () => {
    const mockOnClick = jest.fn();
    const wrapper = shallow(<ChooseFilesButton onClick={mockOnClick} />);

    expect(wrapper.find(MockButtonWrapper).prop('onClick')).toEqual(
      mockOnClick
    );
  });
});
