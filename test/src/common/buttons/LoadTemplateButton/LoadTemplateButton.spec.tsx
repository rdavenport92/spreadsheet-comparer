import React from 'react';
import LoadTemplateButton from '../../../../../src/common/buttons/LoadTemplateButton/LoadTemplateButton';
import { ButtonType } from '../../../../../src/common/buttons/ButtonWrapper';
import { shallow } from 'enzyme';

// eslint-disable-next-line @typescript-eslint/no-explicit-any
let MockButtonWrapper: any;
jest.mock('../../../../../src/common/buttons/ButtonWrapper', () => {
  // eslint-disable-next-line react/display-name
  MockButtonWrapper = (type: ButtonType, label: string | null) => (
    <>{type && label}</>
  );
  return {
    ...(jest.requireActual(
      '../../../../../src/common/buttons/ButtonWrapper'
      // eslint-disable-next-line @typescript-eslint/no-explicit-any
    ) as any),
    __esModule: true,
    default: MockButtonWrapper
  };
});

// eslint-disable-next-line @typescript-eslint/no-explicit-any
let MockDownloadIcon: any;
jest.mock('react-feather', () => {
  // eslint-disable-next-line react/display-name
  MockDownloadIcon = () => <></>;
  return {
    Download: MockDownloadIcon
  };
});

describe('LoadTemplateButton', () => {
  beforeEach(() => {
    jest.resetAllMocks();
  });

  it('should render ButtonWrapper with prop type set to SECONDARY', () => {
    const wrapper = shallow(<LoadTemplateButton />);

    const ButtonWrapper = wrapper.find(MockButtonWrapper);
    // eslint-disable-next-line @typescript-eslint/no-explicit-any
    expect((ButtonWrapper.props() as any).type).toEqual(ButtonType.SECONDARY);
  });

  it('should render label as "Load Template" if includeLabel is set to true', () => {
    const wrapper = shallow(<LoadTemplateButton includeLabel={true} />);

    const ButtonWrapper = wrapper.find(MockButtonWrapper);
    // eslint-disable-next-line @typescript-eslint/no-explicit-any
    expect((ButtonWrapper.props() as any).label).toEqual('Load Template');
  });

  it('should render label as null if includeLabel is set to false', () => {
    const wrapper = shallow(<LoadTemplateButton includeLabel={false} />);

    const ButtonWrapper = wrapper.find(MockButtonWrapper);
    // eslint-disable-next-line @typescript-eslint/no-explicit-any
    expect((ButtonWrapper.props() as any).label).toBeNull();
  });

  it('should render Download icon', () => {
    const wrapper = shallow(<LoadTemplateButton />);

    expect(wrapper.find(MockDownloadIcon)).toHaveLength(1);
  });
});
