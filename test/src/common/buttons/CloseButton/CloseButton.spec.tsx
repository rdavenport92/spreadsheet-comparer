import React from 'react';
import CloseButton from '../../../../../src/common/buttons/CloseButton/CloseButton';
import { ButtonType } from '../../../../../src/common/buttons/ButtonWrapper';
import { shallow } from 'enzyme';

// eslint-disable-next-line @typescript-eslint/no-explicit-any
let MockButtonWrapper: any;
jest.mock('../../../../../src/common/buttons/ButtonWrapper', () => {
  // eslint-disable-next-line react/display-name
  MockButtonWrapper = (type: ButtonType, label: string | null) => (
    <>{type && label}</>
  );
  return {
    ...(jest.requireActual(
      '../../../../../src/common/buttons/ButtonWrapper'
      // eslint-disable-next-line @typescript-eslint/no-explicit-any
    ) as any),
    __esModule: true,
    default: MockButtonWrapper
  };
});

// eslint-disable-next-line @typescript-eslint/no-explicit-any
let MockX: any;
jest.mock('react-feather', () => {
  // eslint-disable-next-line react/display-name
  MockX = () => <></>;
  return {
    X: MockX
  };
});

describe('CloseButton', () => {
  beforeEach(() => {
    jest.resetAllMocks();
  });

  it('should render ButtonWrapper with prop type TRANSPARENT', () => {
    const wrapper = shallow(<CloseButton onClick={jest.fn()} />);

    const button = wrapper.find(MockButtonWrapper);
    expect(button).toHaveLength(1);
    // eslint-disable-next-line @typescript-eslint/no-explicit-any
    expect((button.props() as any).type).toEqual(ButtonType.TRANSPARENT);
  });

  it('should render X icon', () => {
    const wrapper = shallow(<CloseButton onClick={jest.fn()} />);

    expect(wrapper.find(MockX)).toHaveLength(1);
  });

  it('passes onClick to ButtonWrapper', () => {
    const mockOnClick = jest.fn();
    const wrapper = shallow(<CloseButton onClick={mockOnClick} />);

    const buttonWrapper = wrapper.find(MockButtonWrapper);
    expect(buttonWrapper.prop('onClick')).toBe(mockOnClick);
  });
});
