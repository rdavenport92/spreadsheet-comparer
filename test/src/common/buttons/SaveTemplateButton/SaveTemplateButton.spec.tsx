import React from 'react';
import SaveTemplateButton from '../../../../../src/common/buttons/SaveTemplateButton/SaveTemplateButton';
import { ButtonType } from '../../../../../src/common/buttons/ButtonWrapper';
import { shallow } from 'enzyme';

// eslint-disable-next-line @typescript-eslint/no-explicit-any
let MockButtonWrapper: any;
jest.mock('../../../../../src/common/buttons/ButtonWrapper', () => {
  // eslint-disable-next-line react/display-name
  MockButtonWrapper = (type: ButtonType, label: string | null) => (
    <>{type && label}</>
  );
  return {
    ...(jest.requireActual(
      '../../../../../src/common/buttons/ButtonWrapper'
      // eslint-disable-next-line @typescript-eslint/no-explicit-any
    ) as any),
    __esModule: true,
    default: MockButtonWrapper
  };
});

// eslint-disable-next-line @typescript-eslint/no-explicit-any
let MockSaveIcon: any;
jest.mock('react-feather', () => {
  // eslint-disable-next-line react/display-name
  MockSaveIcon = () => <></>;
  return {
    Save: MockSaveIcon
  };
});

describe('SaveTemplateButton', () => {
  beforeEach(() => {
    jest.resetAllMocks();
  });

  it('should render ButtonWrapper with prop type set to SECONDARY', () => {
    const wrapper = shallow(<SaveTemplateButton />);

    const ButtonWrapper = wrapper.find(MockButtonWrapper);
    // eslint-disable-next-line @typescript-eslint/no-explicit-any
    expect((ButtonWrapper.props() as any).type).toEqual(ButtonType.SECONDARY);
  });

  it('should render label as "Save Template" if includeLabel is set to true', () => {
    const wrapper = shallow(<SaveTemplateButton includeLabel={true} />);

    const ButtonWrapper = wrapper.find(MockButtonWrapper);
    // eslint-disable-next-line @typescript-eslint/no-explicit-any
    expect((ButtonWrapper.props() as any).label).toEqual('Save Template');
  });

  it('should render label as null if includeLabel is set to false', () => {
    const wrapper = shallow(<SaveTemplateButton includeLabel={false} />);

    const ButtonWrapper = wrapper.find(MockButtonWrapper);
    // eslint-disable-next-line @typescript-eslint/no-explicit-any
    expect((ButtonWrapper.props() as any).label).toBeNull();
  });

  it('should render Save icon', () => {
    const wrapper = shallow(<SaveTemplateButton />);

    expect(wrapper.find(MockSaveIcon)).toHaveLength(1);
  });
});
