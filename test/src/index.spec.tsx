import React from 'react';
import { renderApp } from '../../src/index';

let mockRender: jest.Mock;
jest.mock('react-dom', () => {
  mockRender = jest.fn();
  return {
    render: mockRender
  };
});

let mockStore: symbol;
jest.mock('../../src/store', () => {
  mockStore = Symbol('mockStore');
  return {
    __esModule: true,
    default: mockStore
  };
});

// eslint-disable-next-line @typescript-eslint/no-explicit-any
let MockProvider: (store: any, children: any) => any;
jest.mock('react-redux', () => {
  // eslint-disable-next-line react/display-name
  MockProvider = ({ store, children }) => (
    <div data-store={store}>{...children}</div>
  );
  return {
    Provider: MockProvider
  };
});

// eslint-disable-next-line @typescript-eslint/no-explicit-any
let MockApp: () => any;
jest.mock('../../src/App', () => {
  // eslint-disable-next-line react/display-name
  MockApp = () => <></>;
  return {
    __esModule: true,
    default: MockApp
  };
});

const mockGetElementById = jest.fn();
const mockGetElementByIdReturnValue = Symbol('mockGetElementByIdReturnValue');

describe('index', () => {
  beforeEach(() => {
    jest.resetAllMocks();
    Object.defineProperty(document, 'getElementById', {
      value: mockGetElementById
    });
    mockGetElementById.mockReturnValue(mockGetElementByIdReturnValue);
  });

  it('should call render with App wrapped in Provider', () => {
    renderApp();

    const component = mockRender.mock.calls[0][0];
    expect(component.type === MockProvider).toBe(true);
    expect(component.props.children.type === MockApp).toBe(true);
  });

  it('should call render with getElementById', () => {
    renderApp();

    expect(mockRender).toHaveBeenCalledWith(
      expect.anything(),
      mockGetElementByIdReturnValue
    );
  });

  it('should getElementById of root', () => {
    renderApp();

    expect(mockGetElementById).toHaveBeenCalledWith('root');
  });

  it('should pass store into Provider', () => {
    renderApp();

    const component = mockRender.mock.calls[0][0];
    expect(component.props.store === mockStore).toBe(true);
  });
});
