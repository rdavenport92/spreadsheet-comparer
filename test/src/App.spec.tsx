import App from '../../src/App';
import { shallow, mount } from 'enzyme';
import React from 'react';

let mockGetPlatform: jest.Mock;
let mockGetIsScreenMaximized: jest.Mock;
let mockSetPlatform: jest.Mock;
let mockSetScreenMaxStatus: jest.Mock;
jest.mock('../../src/common/redux/mainSlice', () => {
  mockGetPlatform = jest.fn();
  mockGetIsScreenMaximized = jest.fn();
  mockSetPlatform = jest.fn();
  mockSetScreenMaxStatus = jest.fn();
  return {
    // eslint-disable-next-line @typescript-eslint/no-explicit-any
    ...(jest.requireActual('../../src/common/redux/mainSlice') as any),
    getPlatform: mockGetPlatform,
    getIsScreenMaximized: mockGetIsScreenMaximized,
    setPlatform: mockSetPlatform,
    setScreenMaxStatus: mockSetScreenMaxStatus
  };
});

// eslint-disable-next-line @typescript-eslint/no-explicit-any
let MockToolbar: any;
jest.mock('../../src/components/Toolbar/Toolbar', () => {
  // eslint-disable-next-line react/display-name
  MockToolbar = () => <></>;
  return {
    __esModule: true,
    default: MockToolbar
  };
});

// eslint-disable-next-line @typescript-eslint/no-explicit-any
let MockHeader: any;
jest.mock('../../src/components/Header/Header', () => {
  // eslint-disable-next-line react/display-name
  MockHeader = () => <></>;
  return {
    __esModule: true,
    default: MockHeader
  };
});

// eslint-disable-next-line @typescript-eslint/no-explicit-any
let MockBody: any;
jest.mock('../../src/components/Body/Body', () => {
  // eslint-disable-next-line react/display-name
  MockBody = () => <></>;
  return {
    __esModule: true,
    default: MockBody
  };
});

let mockUseDispatch: jest.Mock;
let mockUseSelector: jest.Mock;
let mockDispatch: jest.Mock;
jest.mock('react-redux', () => {
  mockUseDispatch = jest.fn();
  mockUseSelector = jest.fn();
  return {
    useDispatch: mockUseDispatch,
    useSelector: mockUseSelector
  };
});

let mockOn: jest.Mock;
let mockSend: jest.Mock;
let mockRemoveListener: jest.Mock;
let mockIpcRenderer: {
  on: jest.Mock;
  send: jest.Mock;
  removeListener: jest.Mock;
};
jest.mock('electron', () => {
  mockOn = jest.fn();
  mockSend = jest.fn();
  mockRemoveListener = jest.fn();
  mockIpcRenderer = {
    on: mockOn,
    send: mockSend,
    removeListener: mockRemoveListener
  };
  return {
    ipcRenderer: mockIpcRenderer
  };
});

describe('App', () => {
  beforeEach(() => {
    jest.resetAllMocks();
    mockDispatch = jest.fn();
    mockUseSelector.mockImplementation((selector) => selector());
    mockUseDispatch.mockImplementation(() => mockDispatch);
    mockSetPlatform.mockImplementation((platform) => platform);
    mockSetScreenMaxStatus.mockImplementation((screenStatus) => screenStatus);
  });

  describe('on mount', () => {
    it('should listen for process-platform and handle response', () => {
      const testPlatform = Symbol('testPlatform');
      mockOn.mockImplementation((channel, cb) => {
        if (channel === 'process-platform') {
          cb({}, testPlatform);
        }
      });

      mount(<App />);

      expect(mockOn).toHaveBeenCalledWith(
        'process-platform',
        expect.anything()
      );
      expect(mockDispatch).toHaveBeenCalledWith(testPlatform);
    });

    it('should listen for screen-max-status and handle response', () => {
      const testScreenMaxStatus = Symbol('testScreenMaxStatus');
      mockOn.mockImplementation((channel, cb) => {
        if (channel === 'screen-max-status') {
          cb({}, testScreenMaxStatus);
        }
      });

      mount(<App />);

      expect(mockOn).toHaveBeenCalledWith(
        'screen-max-status',
        expect.anything()
      );
      expect(mockDispatch).toHaveBeenCalledWith(testScreenMaxStatus);
    });

    it('should send message on get-process-platform channel', () => {
      mount(<App />);

      expect(mockSend).toHaveBeenCalledWith('get-process-platform');
    });

    it('should send message on get-screen-max-status', () => {
      mount(<App />);

      expect(mockSend).toHaveBeenCalledWith('get-screen-max-status');
    });
  });

  describe('on unmount', () => {
    it('should remove process-platform listener', () => {
      const testPlatform = Symbol('testPlatform');
      mockRemoveListener.mockImplementation((channel, cb) => {
        // invoking cb to validate correct function is passed into removeListener
        if (channel === 'process-platform') {
          cb({}, testPlatform);
        }
      });

      const component = mount(<App />);
      component.unmount();

      expect(mockRemoveListener).toHaveBeenCalledWith(
        'process-platform',
        expect.anything()
      );
      expect(mockDispatch).toHaveBeenCalledWith(testPlatform);
    });

    it('should remove screen-max-status listener', () => {
      const testScreenMaxStatus = Symbol('testScreenMaxStatus');
      mockRemoveListener.mockImplementation((channel, cb) => {
        // invoking cb to validate correct function is passed into removeListener
        if (channel === 'screen-max-status') {
          cb({}, testScreenMaxStatus);
        }
      });

      const component = mount(<App />);
      component.unmount();

      expect(mockOn).toHaveBeenCalledWith(
        'screen-max-status',
        expect.anything()
      );
      expect(mockDispatch).toHaveBeenCalledWith(testScreenMaxStatus);
    });
  });

  describe('component', () => {
    it('should always render Header and Body within App container', () => {
      const wrapper = shallow(<App />);
      expect(wrapper.find('.App')).toHaveLength(1);
      expect(wrapper.find(MockHeader)).toHaveLength(1);
      expect(wrapper.find(MockBody)).toHaveLength(1);
    });

    it('should render Toolbar if platform is win32 and maximized', () => {
      mockGetPlatform.mockReturnValue('win32');
      mockGetIsScreenMaximized.mockReturnValue(true);

      const wrapper = shallow(<App />);

      expect(wrapper.find(MockToolbar)).toHaveLength(1);
    });

    it('should render Toolbar if platform is win32 and not maximized', () => {
      mockGetPlatform.mockReturnValue('win32');
      mockGetIsScreenMaximized.mockReturnValue(false);

      const wrapper = shallow(<App />);

      expect(wrapper.find(MockToolbar)).toHaveLength(1);
    });

    it('should render Toolbar if platform is darwin and not maximized', () => {
      mockGetPlatform.mockReturnValue('darwin');
      mockGetIsScreenMaximized.mockReturnValue(false);

      const wrapper = shallow(<App />);

      expect(wrapper.find(MockToolbar)).toHaveLength(1);
    });

    it('should not render Toolbar if platform is darwin and maximized', () => {
      mockGetPlatform.mockReturnValue('darwin');
      mockGetIsScreenMaximized.mockReturnValue(true);

      const wrapper = shallow(<App />);

      expect(wrapper.find(MockToolbar)).toHaveLength(0);
    });
  });
});
