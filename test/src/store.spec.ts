import { setupStore } from '../../src/store';

let mockConfigureStore: jest.Mock;
let mockCombineReducers: jest.Mock;

let mockStore: symbol;
let mockRootReducer: symbol;

jest.mock('@reduxjs/toolkit', () => {
  mockStore = Symbol('store');
  mockRootReducer = Symbol('mockRootReducer');
  mockConfigureStore = jest.fn().mockReturnValue(mockStore);
  mockCombineReducers = jest.fn().mockReturnValue(mockRootReducer);
  return {
    configureStore: mockConfigureStore,
    combineReducers: mockCombineReducers
  };
});

let mockMainReducer: symbol;
jest.mock('../../src/common/redux/mainSlice', () => {
  mockMainReducer = Symbol('mockMainReducer');
  return {
    __esModule: true,
    default: mockMainReducer,
    mainSliceState: {}
  };
});

let mockSpreadsheetsReducer: symbol;
jest.mock('../../src/common/redux/spreadsheetsSlice', () => {
  mockSpreadsheetsReducer = Symbol('mockSpreadsheetsReducer');
  return {
    __esModule: true,
    default: mockSpreadsheetsReducer,
    mainSliceState: {}
  };
});

describe('store', () => {
  beforeEach(() => {
    jest.resetAllMocks();
  });

  it('should pass all appropriate reducers into combineReducers', () => {
    mockCombineReducers.mockImplementation((configObject) => ({
      configObject
    }));

    setupStore();

    expect(mockCombineReducers).toHaveBeenCalledWith(
      expect.objectContaining({
        main: mockMainReducer,
        spreadsheets: mockSpreadsheetsReducer
      })
    );
  });

  it('should call configureStore with appropriate config object', () => {
    mockConfigureStore.mockImplementation((configObject) => ({
      configObject
    }));
    mockCombineReducers.mockReturnValue(mockRootReducer);

    setupStore();

    expect(mockConfigureStore).toHaveBeenCalledWith(
      expect.objectContaining({
        reducer: mockRootReducer
      })
    );
  });
});
