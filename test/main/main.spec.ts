import { isWin, createWindow, main } from '../../main/main';

let originalPlatform: PropertyDescriptor | undefined;

let mockAppOn: jest.Mock;
let mockBrowserWindow: jest.Mock;
let mockIpcMainOn: jest.Mock;
let mockIpcMainRemoveListener: jest.Mock;
let mockApp: { on: jest.Mock };
const mockLoadURL = jest.fn();
const mockLoadFile = jest.fn();
const mockWinOn = jest.fn();
const mockSend = jest.fn();
const mockIsMaximized = jest.fn();
const mockIsFullScreen = jest.fn();
const mockAppRemoveListener = jest.fn();

jest.mock('electron', () => {
  mockAppOn = jest.fn();
  mockBrowserWindow = jest.fn();

  mockIpcMainOn = jest.fn();
  mockIpcMainRemoveListener = jest.fn();
  mockApp = {
    on: mockAppOn
  };

  return {
    app: mockApp,
    BrowserWindow: mockBrowserWindow,
    ipcMain: {
      on: mockIpcMainOn,
      removeListener: mockIpcMainRemoveListener
    }
  };
});

function setPlatform(platform: 'win32' | 'darwin') {
  Object.defineProperty(process, 'platform', {
    value: platform
  });
}

describe('main', () => {
  beforeAll(() => {
    originalPlatform = Object.getOwnPropertyDescriptor(process, 'platform');
  });

  afterAll(() => {
    if (originalPlatform) {
      Object.defineProperty(process, 'platform', originalPlatform);
    }
  });

  beforeEach(() => {
    jest.resetAllMocks();
    // eslint-disable-next-line @typescript-eslint/no-explicit-any
    mockBrowserWindow.mockImplementation(function (this: any) {
      this.loadURL = mockLoadURL;
      this.loadFile = mockLoadFile;
      this.on = mockWinOn;
      this.webContents = {
        send: mockSend
      };
      this.isMaximized = mockIsMaximized;
      this.isFullScreen = mockIsFullScreen;
      this.removeListener = mockAppRemoveListener;
    });
    mockIsMaximized.mockReturnValue('maximized');
    mockIsFullScreen.mockReturnValue('full-screen');
  });

  describe('isWin', () => {
    it('should return true if windows', () => {
      expect(isWin('win32')).toBe(true);
    });

    it('should return false if mac', () => {
      expect(isWin('darwin')).toBe(false);
    });
  });

  describe('createWindow', () => {
    describe('is dev', () => {
      it('should loadURL with localhost:3000', () => {
        createWindow(true);
        expect(mockLoadURL).toHaveBeenCalledWith('http://localhost:3000');
        expect(mockLoadFile).not.toHaveBeenCalled();
      });
    });

    describe('is prod', () => {
      it('should loadFile with index.html', () => {
        createWindow(false);
        expect(mockLoadFile.mock.calls[0][0]).toContain('index.html');
        expect(mockLoadURL).not.toHaveBeenCalled();
      });
    });

    describe('events', () => {
      it('should register get-process-platform event on window creation and call sendProcessPlatform on event', () => {
        mockIpcMainOn.mockImplementation((channel, cb) => {
          if (channel === 'get-process-platform') {
            cb();
          }
        });
        createWindow(true);
        expect(mockIpcMainOn).toHaveBeenCalledWith(
          'get-process-platform',
          expect.anything()
        );
        expect(mockSend).toHaveBeenCalledWith(
          'process-platform',
          process.platform
        );
      });

      it('should register get-screen-max-status event on window creation and call sendScreenMaxStatus on event', () => {
        mockIpcMainOn.mockImplementation((channel, cb) => {
          if (channel === 'get-screen-max-status') {
            cb();
          }
        });
        setPlatform('darwin');
        createWindow(true);
        expect(mockIpcMainOn).toHaveBeenCalledWith(
          'get-screen-max-status',
          expect.anything()
        );
        expect(mockSend).toHaveBeenCalledWith(
          'screen-max-status',
          'full-screen'
        );
      });

      it('should remove event listeners on app quit', () => {
        mockAppOn.mockImplementation((channel, cb) => {
          if (channel === 'quit') {
            cb();
          }
        });
        createWindow(true);
        expect(mockIpcMainRemoveListener).toHaveBeenCalledWith(
          'get-process-platform',
          expect.anything()
        );
        expect(mockIpcMainRemoveListener).toHaveBeenCalledWith(
          'get-screen-max-status',
          expect.anything()
        );
      });
    });

    describe('win32', () => {
      beforeEach(() => {
        setPlatform('win32');
      });

      it('should create window with correct config object', () => {
        createWindow(true);
        expect(mockBrowserWindow).toHaveBeenCalledWith(
          expect.objectContaining({
            width: 800,
            height: 640,
            minWidth: 360,
            frame: false,
            titleBarStyle: 'default',
            webPreferences: { nodeIntegration: true }
          })
        );
      });

      describe('events', () => {
        it('should register maximize listener and call sendScreenMaxStatus on event', () => {
          mockWinOn.mockImplementation((channel, cb) => {
            if (channel === 'maximize') {
              cb();
            }
          });
          createWindow(true);
          expect(mockSend).toHaveBeenCalledWith(
            'screen-max-status',
            'maximized'
          );
        });

        it('should register unmaximize listener and call sendScreenMaxStatus on event', () => {
          mockWinOn.mockImplementation((channel, cb) => {
            if (channel === 'unmaximize') {
              cb();
            }
          });
          createWindow(true);
          expect(mockSend).toHaveBeenCalledWith(
            'screen-max-status',
            'maximized'
          );
        });

        it('should not register darwin specific listeners', () => {
          mockWinOn.mockImplementation((channel, cb) => {
            if (
              channel === 'enter-full-screen' ||
              channel === 'leave-full-screen'
            ) {
              cb();
            }
          });
          createWindow(true);
          expect(mockSend).not.toHaveBeenCalled();
        });

        it('should clean up win32 specific listeners on app quit', () => {
          mockAppOn.mockImplementation((channel, cb) => {
            if (channel === 'quit') {
              cb();
            }
          });
          createWindow(true);
          expect(mockAppRemoveListener).toHaveBeenCalledWith(
            'maximize',
            expect.anything()
          );
          expect(mockAppRemoveListener).toHaveBeenCalledWith(
            'unmaximize',
            expect.anything()
          );
        });

        it('should not attempt to clean up darwin specific listeners on app quit', () => {
          mockAppOn.mockImplementation((channel, cb) => {
            if (channel === 'quit') {
              cb();
            }
          });
          createWindow(true);
          expect(mockAppRemoveListener).not.toHaveBeenCalledWith(
            'enter-full-screen',
            expect.anything()
          );
          expect(mockAppRemoveListener).not.toHaveBeenCalledWith(
            'leave-full-screen',
            expect.anything()
          );
        });
      });
    });

    describe('darwin', () => {
      beforeEach(() => {
        setPlatform('darwin');
      });

      it('should create window with correct config object', () => {
        createWindow(true);
        expect(mockBrowserWindow).toHaveBeenCalledWith(
          expect.objectContaining({
            height: 640,
            minWidth: 360,
            frame: true,
            titleBarStyle: 'hiddenInset',
            webPreferences: { nodeIntegration: true }
          })
        );
      });

      describe('events', () => {
        it('should register enter-full-screen listener and call sendScreenMaxStatus on event', () => {
          mockWinOn.mockImplementation((channel, cb) => {
            if (channel === 'enter-full-screen') {
              cb();
            }
          });
          createWindow(true);
          expect(mockSend).toHaveBeenCalledWith(
            'screen-max-status',
            'full-screen'
          );
        });

        it('should register leave-full-screen listener and call sendScreenMaxStatus on event', () => {
          mockWinOn.mockImplementation((channel, cb) => {
            if (channel === 'leave-full-screen') {
              cb();
            }
          });
          createWindow(true);
          expect(mockSend).toHaveBeenCalledWith(
            'screen-max-status',
            'full-screen'
          );
        });

        it('should not register win32 specific listeners', () => {
          mockWinOn.mockImplementation((channel, cb) => {
            if (channel === 'maximize' || channel === 'unmaximize') {
              cb();
            }
          });
          createWindow(true);
          expect(mockSend).not.toHaveBeenCalled();
        });

        it('should clean up darwin specific listeners on app quit', () => {
          mockAppOn.mockImplementation((channel, cb) => {
            if (channel === 'quit') {
              cb();
            }
          });
          createWindow(true);
          expect(mockAppRemoveListener).toHaveBeenCalledWith(
            'enter-full-screen',
            expect.anything()
          );
          expect(mockAppRemoveListener).toHaveBeenCalledWith(
            'leave-full-screen',
            expect.anything()
          );
        });

        it('should not attempt to clean up darwin specific listeners on app quit', () => {
          mockAppOn.mockImplementation((channel, cb) => {
            if (channel === 'quit') {
              cb();
            }
          });
          createWindow(true);
          expect(mockAppRemoveListener).not.toHaveBeenCalledWith(
            'maximize',
            expect.anything()
          );
          expect(mockAppRemoveListener).not.toHaveBeenCalledWith(
            'unmaximize',
            expect.anything()
          );
        });
      });
    });
  });

  describe('main', () => {
    it('should create window when app is ready', () => {
      let windowCreated = false;
      mockAppOn.mockImplementation((channel, cb) => {
        if (channel === 'ready') {
          const app = cb();
          windowCreated = app === mockApp;
        }
      });
      main();
      expect(windowCreated).toBe(true);
    });
  });
});
