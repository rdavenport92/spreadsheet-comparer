import { app, BrowserWindow, ipcMain, App } from 'electron';
import * as electronIsDev from 'electron-is-dev';

export const isWin = (platform: NodeJS.Platform): boolean =>
  platform === 'win32';

export function createWindow(isDev: boolean): App {
  const win = new BrowserWindow({
    width: 800,
    height: 640,
    minWidth: 360,
    frame: !isWin(process.platform),
    titleBarStyle: isWin(process.platform) ? 'default' : 'hiddenInset',
    webPreferences: {
      nodeIntegration: true
    }
  });

  isDev
    ? win.loadURL('http://localhost:3000')
    : win.loadFile(`${__dirname}/index.html`);

  // Event handlers
  const sendProcessPlatform = () => {
    win.webContents.send('process-platform', process.platform);
  };
  const sendScreenMaxStatus = () => {
    win.webContents.send(
      'screen-max-status',
      isWin(process.platform) ? win.isMaximized() : win.isFullScreen()
    );
  };

  // Event registers
  ipcMain.on('get-process-platform', sendProcessPlatform);
  ipcMain.on('get-screen-max-status', sendScreenMaxStatus);
  if (isWin(process.platform)) {
    win.on('maximize', sendScreenMaxStatus);
    win.on('unmaximize', sendScreenMaxStatus);
  } else {
    win.on('enter-full-screen', sendScreenMaxStatus);
    win.on('leave-full-screen', sendScreenMaxStatus);
  }

  // Cleanup
  app.on('quit', () => {
    ipcMain.removeListener('get-process-platform', sendProcessPlatform);
    ipcMain.removeListener('get-screen-max-status', sendScreenMaxStatus);
    if (isWin(process.platform)) {
      win.removeListener('maximize', sendScreenMaxStatus);
      win.removeListener('unmaximize', sendScreenMaxStatus);
    } else {
      win.removeListener('enter-full-screen', sendScreenMaxStatus);
      win.removeListener('leave-full-screen', sendScreenMaxStatus);
    }
  });

  return app;
}

export function main(): void {
  app.on('ready', () => createWindow(electronIsDev));
}

main();
