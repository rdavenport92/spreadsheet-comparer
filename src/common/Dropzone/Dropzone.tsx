import React from 'react';
import { useDropzone } from 'react-dropzone';
import { File } from 'react-feather';

import ChooseFilesButton from '../buttons/ChooseFilesButton/ChooseFilesButton';
import CloseButton from '../buttons/CloseButton/CloseButton';

import './Dropzone.scss';

interface Props {
  closeable?: boolean | undefined;
  onClose?: () => void;
  onDrop: () => void;
}

const Dropzone: React.FC<Props> = ({ closeable, onClose, onDrop }) => {
  const { getRootProps, getInputProps } = useDropzone({ onDrop });
  const inputProps = getInputProps({ className: 'dropzone-input' });
  const rootProps = getRootProps({ className: 'dropzone' });
  const { onClick } = rootProps;
  delete rootProps.onClick;

  return (
    <div data-testid="dropzone-wrapper" {...rootProps}>
      <input data-testid="dropzone-input" {...inputProps} />
      {closeable && (
        <div className="closeable-wrapper">
          <CloseButton
            onClick={
              onClose ||
              function () {
                console.error('No onClose function was provided');
              }
            }
          />
        </div>
      )}
      <div className="icon-wrapper">
        <File />
      </div>
      <div className="button-text-wrapper">
        <ChooseFilesButton onClick={onClick as () => void} />
        <span className="dropzone-text">or drop file here</span>
      </div>
    </div>
  );
};

export default Dropzone;
