import React from 'react';
import './Hr.scss';

const Hr: React.FC<unknown> = () => <hr className="hr" data-testid="hr"></hr>;

export default Hr;
