import React from 'react';
import { Menu } from 'react-feather';

import ButtonWrapper, { ButtonType } from '../ButtonWrapper';

const MenuButton: React.FC<unknown> = () => (
  <ButtonWrapper
    onClick={function () {
      return;
    }}
    type={ButtonType.SECONDARY}
  >
    <Menu />
  </ButtonWrapper>
);

export default MenuButton;
