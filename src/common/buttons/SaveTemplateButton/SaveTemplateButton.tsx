import React from 'react';
import { Save } from 'react-feather';

import ButtonWrapper, { ButtonType } from '../ButtonWrapper';

interface Props {
  includeLabel?: boolean;
}

const SaveTemplateButton: React.FC<Props> = ({ includeLabel }) => (
  <ButtonWrapper
    onClick={function () {
      return;
    }}
    type={ButtonType.SECONDARY}
    label={includeLabel ? 'Save Template' : null}
  >
    <Save />
  </ButtonWrapper>
);

export default SaveTemplateButton;
