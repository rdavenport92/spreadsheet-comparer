import React from 'react';

import ElectronService from '../../../shared/ElectronService/ElectronService';

import './Win32ButtonWrapper.scss';

export enum Win32ButtonType {
  CLOSE = 'close',
  MAX = 'max',
  UNMAX = 'unmax',
  MIN = 'min'
}
interface Props {
  type: Win32ButtonType;
}

const electronService = new ElectronService();

const clickHandler = (type: Win32ButtonType) => {
  switch (type) {
    case Win32ButtonType.CLOSE:
      electronService.closeWindow();
      break;
    case Win32ButtonType.MAX:
      electronService.maximizeWindow();
      break;
    case Win32ButtonType.UNMAX:
      electronService.unmaximizeWindow();
      break;
    case Win32ButtonType.MIN:
      electronService.minimizeWindow();
      break;
    default:
      break;
  }
};

const Win32ButtonWrapper: React.FC<Props> = ({ children, type }) => {
  return (
    <div onClick={() => clickHandler(type)} className="win32-btn-wrapper">
      {children}
    </div>
  );
};

export default Win32ButtonWrapper;
