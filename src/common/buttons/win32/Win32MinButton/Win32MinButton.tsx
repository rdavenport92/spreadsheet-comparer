import React from 'react';
import { Minus } from 'react-feather';

import Win32ButtonWrapper, { Win32ButtonType } from '../Win32ButtonWrapper';

import './Win32MinButton.scss';

const Win32MinButton: React.FC<unknown> = () => (
  <Win32ButtonWrapper type={Win32ButtonType.MIN}>
    <div className="win32-min-btn-wrapper">
      <Minus />
    </div>
  </Win32ButtonWrapper>
);

export default Win32MinButton;
