import React from 'react';
import { X } from 'react-feather';

import Win32ButtonWrapper, { Win32ButtonType } from '../Win32ButtonWrapper';

import './Win32CloseButton.scss';

const Win32CloseButton: React.FC<unknown> = () => (
  <Win32ButtonWrapper type={Win32ButtonType.CLOSE}>
    <div className="win32-close-btn-wrapper">
      <X />
    </div>
  </Win32ButtonWrapper>
);

export default Win32CloseButton;
