import React from 'react';
import { Square } from 'react-feather';

import Win32ButtonWrapper, { Win32ButtonType } from '../Win32ButtonWrapper';

import './Win32UnmaxButton.scss';

const Win32UnmaxButton: React.FC<unknown> = () => (
  <Win32ButtonWrapper type={Win32ButtonType.UNMAX}>
    <div className="win32-unmax-btn-wrapper">
      <div className="win32-unmax-square-1">
        <Square />
      </div>
      <div className="win32-unmax-square-2">
        <Square />
      </div>
    </div>
  </Win32ButtonWrapper>
);

export default Win32UnmaxButton;
