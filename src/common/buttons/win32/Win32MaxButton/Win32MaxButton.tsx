import React from 'react';
import { Square } from 'react-feather';

import Win32ButtonWrapper, { Win32ButtonType } from '../Win32ButtonWrapper';

import './Win32MaxButton.scss';

const Win32MaxButton: React.FC<unknown> = () => (
  <Win32ButtonWrapper type={Win32ButtonType.MAX}>
    <div className="win32-max-btn-wrapper">
      <div className="square-wrapper">
        <Square />
      </div>
    </div>
  </Win32ButtonWrapper>
);

export default Win32MaxButton;
