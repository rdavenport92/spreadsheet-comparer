import React from 'react';
import ButtonWrapper, { ButtonType } from '../ButtonWrapper';

interface Props {
  onClick: () => void;
}

const ChooseFilesButton: React.FC<Props> = ({ onClick }) => (
  <ButtonWrapper
    onClick={onClick}
    type={ButtonType.PRIMARY}
    label="Choose Files"
  ></ButtonWrapper>
);

export default ChooseFilesButton;
