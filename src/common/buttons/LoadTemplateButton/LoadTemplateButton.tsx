import React from 'react';
import { Download } from 'react-feather';

import ButtonWrapper, { ButtonType } from '../ButtonWrapper';

interface Props {
  includeLabel?: boolean;
}

const LoadTemplateButton: React.FC<Props> = ({ includeLabel }) => (
  <ButtonWrapper
    onClick={function () {
      return;
    }}
    type={ButtonType.SECONDARY}
    label={includeLabel ? 'Load Template' : null}
  >
    <Download />
  </ButtonWrapper>
);

export default LoadTemplateButton;
