import React from 'react';
import { X } from 'react-feather';

import ButtonWrapper, { ButtonType } from '../ButtonWrapper';

interface Props {
  onClick: () => void;
}

const CloseButton: React.FC<Props> = ({ onClick }) => (
  <ButtonWrapper onClick={onClick} type={ButtonType.TRANSPARENT}>
    <X />
  </ButtonWrapper>
);

export default CloseButton;
