import Button from '@material/react-button';
import React from 'react';

import './ButtonWrapper.scss';
import '@material/react-button/index.scss';

export enum ButtonType {
  PRIMARY = 'primary',
  SECONDARY = 'secondary',
  TRANSPARENT = 'transparent'
}

interface Props {
  onClick: () => void;
  strong?: boolean;
  type: ButtonType;
  label?: string | null | undefined;
}

const ButtonWrapper: React.FC<Props> = ({
  onClick,
  type,
  label,
  children,
  strong
}) => (
  <Button
    className={`${
      type === ButtonType.PRIMARY
        ? 'button-wrapper-primary'
        : type === ButtonType.SECONDARY
        ? 'button-wrapper-secondary'
        : 'button-wrapper-transparent'
    } ${strong && 'button-wrapper-stretch'}`}
    onClick={onClick}
  >
    <div className="children-container">
      <div className="button-icon">{children}</div>
      {children && label ? <div className="spacer" /> : <></>}
      <div
        className="button-label"
        style={strong ? { textTransform: 'uppercase', fontWeight: 'bold' } : {}}
      >
        {label}
      </div>
    </div>
  </Button>
);

export default ButtonWrapper;
