import React from 'react';
import { CornerDownLeft } from 'react-feather';

import ButtonWrapper, { ButtonType } from '../ButtonWrapper';

interface Props {
  includeLabel?: boolean;
}

const ExecuteComparisonButton: React.FC<Props> = ({ includeLabel }) => (
  <ButtonWrapper
    onClick={function () {
      return;
    }}
    type={ButtonType.PRIMARY}
    label={includeLabel ? 'Compare' : null}
    strong={!!includeLabel}
  >
    <CornerDownLeft />
  </ButtonWrapper>
);

export default ExecuteComparisonButton;
