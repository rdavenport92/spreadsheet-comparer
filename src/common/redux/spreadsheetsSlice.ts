import { createSlice, Slice } from '@reduxjs/toolkit';
import { RootState } from '../../store';

interface Spreadsheet {
  filename: string;
}

export interface SpreadsheetsSliceState {
  master: Spreadsheet | null;
  children: Spreadsheet[];
}

export const initialState: SpreadsheetsSliceState = {
  master: null,
  children: []
};

export const reducers = {
  setMasterSpreadsheet: (
    state: SpreadsheetsSliceState,
    action: { payload: string }
  ): SpreadsheetsSliceState => ({
    ...state,
    master: { filename: action.payload }
  }),
  addChildSpreadsheet: (
    state: SpreadsheetsSliceState,
    action: { payload: string }
  ): SpreadsheetsSliceState => {
    const currentChildren = [...state.children];
    currentChildren.push({ filename: action.payload });
    return { ...state, children: currentChildren };
  }
};

export const setupSlice = (): Slice<SpreadsheetsSliceState> =>
  createSlice({
    name: 'spreadsheets',
    initialState,
    reducers
  });

export const spreadsheetsSlice = setupSlice();

export const {
  setMasterSpreadsheet,
  addChildSpreadsheet
} = spreadsheetsSlice.actions;

export const getMasterSpreadsheet = (state: RootState): Spreadsheet | null =>
  state.spreadsheets.master;
export const getChildrenSpreadsheets = (state: RootState): Spreadsheet[] =>
  state.spreadsheets.children;

export default spreadsheetsSlice.reducer;
