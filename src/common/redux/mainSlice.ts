import { createSlice, Slice } from '@reduxjs/toolkit';
import { RootState } from '../../store';

export enum Platform {
  WIN32 = 'win32',
  DARWIN = 'darwin'
}

export interface MainSliceState {
  platform: Platform | null;
  isScreenMaximized: boolean | null;
}

export const initialState: MainSliceState = {
  platform: null,
  isScreenMaximized: null
};

export const reducers = {
  setPlatform: (
    state: MainSliceState,
    action: { payload: Platform; type: string }
  ): MainSliceState => ({
    ...state,
    platform: action.payload
  }),
  setScreenMaxStatus: (
    state: MainSliceState,
    action: { payload: boolean; type: string }
  ): MainSliceState => ({
    ...state,
    isScreenMaximized: action.payload
  })
};

export const setupSlice = (): Slice<MainSliceState> =>
  createSlice({
    name: 'main',
    initialState,
    reducers
  });

export const mainSlice = setupSlice();

// Actions
export const { setPlatform, setScreenMaxStatus } = mainSlice.actions;

// Selectors
export const getPlatform = (state: RootState): Platform | null =>
  state.main.platform;
export const getIsScreenMaximized = (state: RootState): boolean | null =>
  state.main.isScreenMaximized;

export default mainSlice.reducer;
