import {
  CombinedState,
  combineReducers,
  Reducer,
  configureStore,
  EnhancedStore
} from '@reduxjs/toolkit';
import mainReducer, { MainSliceState } from './common/redux/mainSlice';
import spreadsheetsReducer, {
  SpreadsheetsSliceState
} from './common/redux/spreadsheetsSlice';

interface RootReducerState {
  main: MainSliceState;
  spreadsheets: SpreadsheetsSliceState;
}

type RootReducer = Reducer<CombinedState<RootReducerState>>;
type Store = EnhancedStore<CombinedState<RootReducerState>>;

interface StoreSetupReturnObj {
  rootReducer: RootReducer;
  store: Store;
}

export const setupStore = (): StoreSetupReturnObj => {
  const rootReducer = combineReducers<RootReducerState>({
    main: mainReducer,
    spreadsheets: spreadsheetsReducer
  });
  const store = configureStore({ reducer: rootReducer });
  return { rootReducer, store };
};

const { rootReducer, store } = setupStore();

export type RootState = ReturnType<typeof rootReducer>;
export default store;
