import { ipcRenderer } from 'electron';
import React, { useEffect } from 'react';
import { useDispatch, useSelector } from 'react-redux';
import './App.scss';
import Body from './components/Body/Body';
import Header from './components/Header/Header';
import Toolbar from './components/Toolbar/Toolbar';
import {
  getIsScreenMaximized,
  Platform,
  setPlatform,
  setScreenMaxStatus,
  getPlatform
} from './common/redux/mainSlice';

const App: React.FC = () => {
  const dispatch = useDispatch();

  const platform = useSelector(getPlatform);
  const isScreenMaximized = useSelector(getIsScreenMaximized);

  const handleSetPlatform = (
    event: Electron.IpcRendererEvent,
    platform: Platform
  ) => {
    dispatch(setPlatform(platform));
  };

  const handleSetScreenMaxStatus = (
    event: Electron.IpcRendererEvent,
    screenStatus: boolean
  ) => {
    dispatch(setScreenMaxStatus(screenStatus));
  };

  useEffect(() => {
    ipcRenderer.on('process-platform', handleSetPlatform);
    ipcRenderer.on('screen-max-status', handleSetScreenMaxStatus);

    ipcRenderer.send('get-process-platform');
    ipcRenderer.send('get-screen-max-status');

    return () => {
      ipcRenderer.removeListener('process-platform', handleSetPlatform);
      ipcRenderer.removeListener('screen-max-status', handleSetScreenMaxStatus);
    };
  }, []);

  return (
    <div className="App">
      {(platform === Platform.WIN32 ||
        (Platform.DARWIN && !isScreenMaximized)) && <Toolbar />}
      <Header />
      <Body />
    </div>
  );
};

export default App;
