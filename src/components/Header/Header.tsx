import React from 'react';

import SaveTemplateButton from '../../common/buttons/SaveTemplateButton/SaveTemplateButton';
import LoadTemplateButton from '../../common/buttons/LoadTemplateButton/LoadTemplateButton';
import ExecuteComparisonButton from '../../common/buttons/ExecuteComparisonButton/ExecuteComparisonButton';
import MenuButton from '../../common/buttons/MenuButton/MenuButton';

import './Header.scss';

const Header: React.FC<unknown> = () => (
  <div className="header-wrapper">
    <div className="menu-button-container">
      <MenuButton />
    </div>
    <div className="buttons-container">
      <div className="button-wrapper-right">
        <SaveTemplateButton />
      </div>
      <div className="button-wrapper-right">
        <LoadTemplateButton />
      </div>
      <div className="button-wrapper-right">
        <ExecuteComparisonButton />
      </div>
    </div>
  </div>
);

export default Header;
