import React from 'react';
import { useSelector } from 'react-redux';

import Win32CloseButton from '../../../common/buttons/win32/Win32CloseButton/Win32CloseButton';
import Win32MaxButton from '../../../common/buttons/win32/Win32MaxButton/Win32MaxButton';
import Win32MinButton from '../../../common/buttons/win32/Win32MinButton/Win32MinButton';
import Win32UnmaxButton from '../../../common/buttons/win32/Win32UnmaxButton/Win32UnmaxButton';
import { getIsScreenMaximized } from '../../../common/redux/mainSlice';

import './Win32Controls.scss';

const Win32Controls: React.FC<unknown> = () => {
  const isScreenMaximized = useSelector(getIsScreenMaximized);
  return (
    <div className="win32controls-container">
      <Win32MinButton />
      {isScreenMaximized ? <Win32UnmaxButton /> : <Win32MaxButton />}
      <Win32CloseButton />
    </div>
  );
};

export default Win32Controls;
