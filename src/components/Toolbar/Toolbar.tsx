import React from 'react';
import { useSelector } from 'react-redux';

import { getPlatform } from '../../common/redux/mainSlice';
import Win32Controls from './Win32Controls/Win32Controls';

import './Toolbar.scss';

const Toolbar: React.FC<unknown> = () => {
  const platform = useSelector(getPlatform);

  return (
    <div className="toolbar-wrapper">
      {platform === 'win32' && <Win32Controls />}
    </div>
  );
};

export default Toolbar;
