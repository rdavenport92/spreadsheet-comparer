import React from 'react';
import Hr from '../../../../common/Hr/Hr';
import Dropzone from '../../../../common/Dropzone/Dropzone';

import './FileSection.scss';

export enum FileSectionType {
  MASTER = 'master',
  CHILDREN = 'children'
}

interface Props {
  type: FileSectionType;
}

const FileSection: React.FC<Props> = ({ type }) => (
  <div className="file-section-wrapper">
    <div className="file-section-header">
      {type === FileSectionType.MASTER ? 'Master' : 'Children'}
    </div>
    <div className="file-section-content">
      {type === FileSectionType.MASTER && <Dropzone onDrop={console.log} />}
    </div>
    <div className="file-section-hr-wrapper">
      <Hr />
    </div>
  </div>
);

export default FileSection;
