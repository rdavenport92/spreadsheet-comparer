import React from 'react';

import ExecuteComparisonButton from '../../../common/buttons/ExecuteComparisonButton/ExecuteComparisonButton';
import FileSection, { FileSectionType } from './FileSection/FileSection';

import './SpreadSheetsPanel.scss';

const SpreadSheetsPanel: React.FC<unknown> = () => (
  <div className="spreadsheets-panel-wrapper">
    <FileSection type={FileSectionType.MASTER} />
    <FileSection type={FileSectionType.CHILDREN} />
    <div className="execute-button-wrapper">
      <ExecuteComparisonButton includeLabel={true} />
    </div>
  </div>
);

export default SpreadSheetsPanel;
