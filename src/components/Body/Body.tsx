import React from 'react';
import './Body.scss';

import SectionPanel from './SectionPanel/SectionPanel';
import SpreadSheetsPanel from './SpreadSheetsPanel/SpreadSheetsPanel';

const Body: React.FC<unknown> = () => (
  <div className="body-wrapper">
    <SectionPanel title="Spreadsheets">
      <SpreadSheetsPanel />
    </SectionPanel>
  </div>
);

export default Body;
