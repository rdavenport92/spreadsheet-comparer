import React from 'react';
import './SectionPanel.scss';

interface Props {
  title: string;
}

const SectionPanel: React.FC<Props> = ({ title, children }) => (
  <div className="section-panel-wrapper">
    <div className="section-panel-header">{title}</div>
    {children}
  </div>
);

export default SectionPanel;
