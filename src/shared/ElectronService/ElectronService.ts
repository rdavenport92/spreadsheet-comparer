import { remote } from 'electron';

export default class ElectronService {
  get currentWindow(): Electron.BrowserWindow {
    return remote.getCurrentWindow();
  }

  maximizeWindow(): void {
    this.currentWindow.maximize();
  }

  unmaximizeWindow(): void {
    this.currentWindow.unmaximize();
  }

  minimizeWindow(): void {
    this.currentWindow.minimize();
  }

  closeWindow(): void {
    this.currentWindow.close();
  }
}
